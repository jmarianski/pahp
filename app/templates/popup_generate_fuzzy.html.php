<style>
    input {
        width:100%;
        padding:0;
        margin:0;
    }
    td {
        padding-left:3px;
        padding-right:3px;
    }
    tr:nth-child(1) td {
        text-align: center;
    }
</style>
<h1><?=t("popup_fuzzy_header")?></h1>
<?=t("popup_fuzzy_description")?>
<table border="1" style="width:100%">
    <tr>
        <td><?=t("popup_fuzzy_lower")?></td>
        <td><?=t("popup_fuzzy_middle")?></td>
        <td><?=t("popup_fuzzy_upper")?></td>
        <td><?=t("popup_fuzzy_delete")?></td>
    </tr>
    <tr>
        <td>
            <button type="button" id="add"><?=t("popup_fuzzy_add")?></button>
        <input type="hidden" name="value"></td>
        <td><button id="finish" type="button"><?=t("popup_fuzzy_finish")?></button></td>
        <td></td>
        <td></td>
    </tr>
</table>
<script>
    var data = <?
    if(is_array(json_decode($val)))
        echo $val;
    else
        echo "[]";
        ?>;
    if(data.length>0)
        for(var i=0; i<data.length; i++) {
            add_row(data[i]);
        }
    else
        add_row();
    $("button#add").click(add_row);
    function add_row(row) {
        var html = $("<tr></tr>");
        
        if(typeof row != 'undefined')
            for(var i=0; i<3; i++)
                html.append("<td><input value=\""+row[i]+"\"></td>");
        else
            for(var i=0; i<3; i++)
                html.append("<td><input value=1></td>");
        
        html.append("<td><button type='button'>-</button></td>");
        html.find("input").on("change", input_onchange);
        html.find("button").click(button_minus);
        $("table tr:last-child").before(html);
    }
    function input_onchange() {
        
    }
    function button_minus() {
        if($(this).parents("table").find("tr").length>3)
            $(this).parents("tr").remove();
        else {
            $(this)
                    .parents("table")
                    .find("tr:nth-child(2) input")
                    .val("1");
        }
    }
    $("button#finish").click(function() {
        var vals = [];
        $("table tr").each(function(index, row) {
            var row_vals = [];
            $(row).find("input").each(function (index2, input) {
                if($(input).val() >0 )
                    row_vals.push(parseFloat($(input).val()));
            });
            if(row_vals.length==3 && row_vals[0]<=row_vals[1] && row_vals[1] <= row_vals[2])
                vals.push(row_vals);
        });
        var ser = JSON.stringify(vals);
        if(vals.length>0)
            $("input[name='value']").val(ser);
        self.close();
    });    
</script>