<?php
$urls = array(
    array(
        "http://www.sciencedirect.com/science/article/pii/0377221795003002",
        "http://www.sciencedirect.com/science/article/pii/S0888613X10001611",
        "http://www.sciencedirect.com/science/article/pii/S0957417410003660",
        "http://www.sciencedirect.com/science/article/pii/S0301479709001327",
        "http://www.sciencedirect.com/science/article/pii/S0957417413004958"
    ),
    array(
        "http://www.sciencedirect.com/science/article/pii/S0377221713008576",
        "http://www.sciencedirect.com/science/article/pii/S0888613X10001611"
    ),
);
$citations = array(
    array(
        "Chang, Da-Yong. \"Applications of the extent analysis method on fuzzy AHP.\" European journal of operational research 95.3 (1996): 649-655.",
        "Wang, Ying-Ming, and Kwai-Sang Chin. \"Fuzzy analytic hierarchy process: A logarithmic fuzzy preference programming methodology.\" International Journal of Approximate Reasoning 52.4 (2011): 541-553",
        "Sun, Chia-Chi. \"A performance evaluation model by integrating fuzzy AHP and fuzzy TOPSIS methods.\" Expert systems with applications 37.12 (2010): 7745-7754.",
        "Vahidnia, Mohammad H., Ali A. Alesheikh, and Abbas Alimohammadi. \"Hospital site selection using fuzzy AHP and its derivatives.\" Journal of environmental management 90.10 (2009): 3048-3056.",
        "Deng, Xinyang, et al. \"Supplier selection using AHP methodology extended by D numbers.\" Expert Systems with Applications 41.1 (2014): 156-167.",
        ),
    array(
        "Zhü, Kèyù. \"Fuzzy analytic hierarchy process: Fallacy of the popular methods.\" European Journal of Operational Research 236.1 (2014): 209-217.",
        "Wang, Ying-Ming, Ying Luo, and Zhongsheng Hua. \"On the extent analysis method for fuzzy AHP and its applications.\" European Journal of Operational Research 186.2 (2008): 735-747."
    )
);
$titles = t("science_titles");
foreach($urls as $key=>$array) {
    echo "<h3>".$titles[$key]."</h3>";
    foreach($array as $key2=>$value2) {
        $cit = $citations[$key][$key2];
        echo "<p><a href=\"$value2\">$value2</a> - $cit</p>";
    }
}