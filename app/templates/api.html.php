<div class="content"><?php
$section_text = t("api_section_values");
$algorithms = t("advanced_algorithms");
$prepend = array();
$append_after = array();
$prepend[0][1] = "<i><b>num_crit</b></i> - ";
$prepend[0][2] = "<i><b>num_pref</b></i> - ";
$prepend[0][3] = "<i><b>names</b></i> - ";
$prepend[1][5] = "<i><b>class</b></i> - ";
$prepend[1][6] = "<i><b>points</b></i> - ";
$prepend[1][7] = "<i><b>h</b></i> - ";
$prepend[1][8] = "<i><b>min</b></i> - ";
$prepend[1][9] = "<i><b>max</b></i> - ";
$prepend[1][10] = "<i><b>step</b></i> - ";
$prepend[2][1] = "<i><b>algo</b></i> - ";
$prepend[2][2] = "<i><b>params</b></i> - ";
$append_after[0][3] = "<pre>\"names\":{\"criteria\":{\"0\":\"crit1\",\"1\":\"crit2\"},\"objects\":{\"0\":\"obj1\",\"1\":\"obj2\",\"2\":\"obj3\"}}</pre>";
$append_after[1][1] = "<pre>\"matrices\": {\"matrix\":{\n\"0\":{\"type\":\"regular\",\"data\":{\"0\":{\"0\":\"1\",\"1\":\"1\"},\"1\":{\"0\":\"1\",\"1\":\"1\"}}},\n\"1\":{\"data\":{\"0\":{\"class\":\"Gauss\",\"points\":\"1,2,3\",\"h\":\"0.5\",\"min\":\"-10\",\"max\":\"10\",\"step\":\"0.1\"},\"1\":{\"class\":\"Epanechnikov\"},\"2\":{\"class\":\"Triangular\"}},\"type\":\"func\"},\n\"2\":{\"type\":\"regular\",\"data\":{\"0\":{\"0\":\"1\",\"1\":\"3\",\"2\":\"1\"},\"1\":{\"0\":\"0.333333\",\"1\":\"1\",\"2\":\"3\"},\"2\":{\"0\":\"1\",\"1\":\"0.333333\",\"2\":\"1\"}}}}}</pre>";
$append_after[2][1] = "<ul>";
$append_after[2][2] = "<ul>";
$examples = array(
    '{"base": {"num_crit":"2","num_pref":"3","names":{"criteria":{"0":"crit1","1":"crit2"},"objects":{"0":"obj1","1":"obj2","2":"obj3"}}}, "matrices": {"matrix":{"0":{"type":"regular","data":{"0":{"0":"1","1":"1"},"1":{"0":"1","1":"1"}}},"1":{"data":{"0":{"class":"Gauss","points":"1,2,3","h":"0.5","min":"-10","max":"10","step":"0.1"},"1":{"class":"Epanechnikov"},"2":{"class":"Triangular"}},"type":"func"},"2":{"type":"regular","data":{"0":{"0":"1","1":"3","2":"1"},"1":{"0":"0.333333","1":"1","2":"3"},"2":{"0":"1","1":"0.333333","2":"1"}}}}}, "algorithms": {"algo":{"0":"AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo","4":"AHP\\\\Algorithms\\\\Extractors\\\\JsonExtractor"},"params":{"AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo":{"n":"1000"}}}}',
    '{"base": {"num_crit":"1","num_pref":"2"}, "matrices": {"matrix":{"0":{"type":"regular","data":{"0":{"0":"1"}}},"1":{"type":"regular","data":{"0":{"0":"1","1":"0.5"},"1":{"0":"2","1":"1"}}}}}, "algorithms": {"algo":{"0":"AHP\\\\Algorithms\\\\Solvers\\\\Standard","4":"AHP\\\\Algorithms\\\\Extractors\\\\PrettyPrint"}}}',
);
$examples_text = t("api_examples_text");
foreach($classes as $key=>$array) {
    $algo = str_replace("$", $algorithms[$key], t("api_algorithm"));
    $append_after[2][1] .= "<li>[$key] - $algo. ".t("api_subclasses").":<ul>";
    foreach($array as $class) {
        $name = $class["name"];
        $append_after[2][1].= "<li>$name</li>";
        if(count($class["params"])>0) {
            $append_after[2][2].= "<li>$name<ul>";
            foreach($class["params"] as $p) {
                $append_after[2][2].= "<li>$p</li>";
            }
            $append_after[2][2].= "</ul></li>";
        }
    }
    $append_after[2][1].= "</ul></li>";
}
$append_after[2][1] .="</ul>";
$append_after[2][2] .="</ul>";
foreach(t("api_description") as $i=>$desc) {
    if($i==0) {
        $url = substr($url, 0, -4)."/ahp";
        $ahref = "<a href=\"$url\">$url</a>";
        $desc = str_replace("$", $ahref, $desc);
        $desc .= "<ul><li>base</li><li>matrices</li><li>algorithms</li></ul>";
    }
    echo "<p>$desc</p>\n";
}
foreach(t("api_section_names") as $i=>$section) {
    echo "<h2>$section</h2>\n";
    foreach($section_text[$i] as $j=>$text) {
        if(isset($prepend[$i][$j]))
            $text = $prepend[$i][$j].$text;
        echo "<p>$text</p>";
        if(isset($append_after[$i][$j]))
            echo $append_after[$i][$j];
    }
    if($i<3 && $i!=1)
    echo "<h3>".t("api_section_examples")."</h3>";
    switch($i) {
        case 0:
            echo "<pre>\"base\": {\"num_crit\":\"2\",\"num_pref\":\"3\",\"names\":{\"criteria\":{\"0\":\"crit1\",\"1\":\"crit2\"},\"objects\":{\"0\":\"obj1\",\"1\":\"obj2\",\"2\":\"obj3\"}}}</pre>";
            break;
        case 2:
            echo "<pre>\"algorithms\": {\"algo\":{\"0\":\"AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo\",\"4\":\"AHP\\\\Algorithms\\\\Extractors\\\\JsonExtractor\"},\"params\":{\"AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo\":{\"n\":\"1000\"}}}</pre>";
            break;
    }
}
echo "<h2>".t("api_examples")."</h2>";
foreach($examples as $key=>$e) {
    echo "<pre>$e</pre>";
    if(isset($examples_text[$key]))
        echo "<p>".$examples_text[$key]."</p>";
}
?>
</div>