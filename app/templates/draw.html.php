<?=t("draw_message")?>
<form>
    <select name="class">
        <option value="Gauss">Gauss</option>
        <option value="Dirac">Dirac</option>
        <option value="Epanechnikov">Epanechnikov</option>
        <option value="Triangular">Triangular</option>
    </select>
    <select name="method">
        <option value="pdf">PDF</option>
        <option value="cdf">CDF</option>
    </select>
    <input name="points" placeholder="<?=t("chart_points")?>">
    <input name="h" placeholder="<?=t("chart_smooth")?>">
    <input name="min" placeholder="<?=t("chart_min")?>">
    <input name="max" placeholder="<?=t("chart_max")?>">
    <input name="step" placeholder="<?=t("chart_step")?>">
    <button type="button"><?=t("chart_show")?></button>
    <BR>
    <img style="display: none" src="web/assets/images/loading.gif">
</form>
<script>
    $("button").on("click", function() {
        var form = $(this).parent();
        var class1 = form.find("select[name='class']").val();
        var method = form.find("select[name='method']").val();
        var points = form.find("input[name='points']").val();
        var h = form.find("input[name='h']").val();
        var min = form.find("input[name='min']").val();
        var max = form.find("input[name='max']").val();
        var step = form.find("input[name='step']").val();
        var url = "chart?class="+class1+"&method="+method+"&points="+points
                +"&h="+h+"&min="+min+"&max="+max+"&step="+step;
        var img = $(this).parent().find("img");
        img.css("display", "block");
        img.attr("src", "web/assets/images/loading.gif");
        var load = new Image();
        load.onload = function() {
            img.attr("src", url);
        };
        load.src = url;
    });
</script>