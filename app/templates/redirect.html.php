
<div class="content">
        <?php
        $urls = t("top_url");
        function url2($u) {
            return "/pahp/".$u;
        }
        function find_url($look, $data) {
            foreach($data as $url=>$d) {
                if($look==$url)
                    return $d;
                if(is_array($d)) {
                    $result = find_url($d["data"]);
                    if($result!=null)
                        return $result;
                }
            }
            return null;
        }
        function display_urls($data) {
            foreach($data as $url=>$d) {
                if(is_array($d)) {
                    echo "<li class=\"has-submenu\">";
                    echo "<b><a href=\"".url($url)."\">".$d["name"]."</a></b>";
                    echo "<ul>";
                    display_urls($d["data"]);
                    echo "</ul>";
                } else {
                    echo "<li>";
                    echo "<a href=\"".url2($url)."\">$d</a>";
                }
                echo "</li>";
            }
        }
        $this_url = explode("/", $url);
        $this_url = $this_url[count($this_url)-1];
        $looked = find_url($this_url, $urls);
        if(!is_array($looked))
            echo "There was an error seems like this page exists but doesn't contain any content, even though it was marked as redirector.";
        else {
            echo "<ul>";
            display_urls($looked["data"]);
            echo "</ul>";
        }
        ?>
</div>