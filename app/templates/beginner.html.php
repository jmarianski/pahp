<script src="web/assets/scripts/formToObject.min.js"></script>
<style>
        .form_func_hidden{
            display: none;
        }
        .matrix {
            background-color: rgba(0,0,0,0.05) 
        }

.select {
    position: relative;
}

.tp {
    display: none;
    padding: 20px;
}

.select .tp {
    position:absolute;
}

.select:hover .tp {
    background: white;
    border-radius: 4px;
    border: 1px double #9999dd;
    top: 49px;
    color: black;
    display: inline;
    left: 20px;
    padding: 10px;
    position: absolute;
    z-index: 1000;
}

</style>
<h2 class="header">Step header</h2>
<div class="text_content"></div>
<div class="step[3] step[4] pairwise_helper">
    <div class="pairwise">prompt</div>
    <table class="pairwise">
        <tr>
            <td class="object_1" rowspan="2">O1</td>
            <td class='select'>9<span class="tp"></span></td>
            <td class='select'>7<span class="tp"></span></td>
            <td class='select'>5<span class="tp"></span></td>
            <td class='select'>3<span class="tp"></span></td>
            <td class='select'>1<span class="tp"></span></td>
            <td class='select'>3<span class="tp"></span></td>
            <td class='select'>5<span class="tp"></span></td>
            <td class='select'>7<span class="tp"></span></td>
            <td class='select'>9<span class="tp"></span></td>
            <td class="object_2" rowspan="2">O2</td>
        </tr>
        <tr>
            <td><input type="radio" name="pairwise" value="9"></td>
            <td><input type="radio" name="pairwise" value="7"></td>
            <td><input type="radio" name="pairwise" value="5"></td>
            <td><input type="radio" name="pairwise" value="3"></td>
            <td><input type="radio" name="pairwise" value="1"></td>
            <td><input type="radio" name="pairwise" value="2"></td>
            <td><input type="radio" name="pairwise" value="4"></td>
            <td><input type="radio" name="pairwise" value="6"></td>
            <td><input type="radio" name="pairwise" value="8"></td>
        </tr>
    </table>
    <div><input type="checkbox" name="dont_block_buttons"> <?=t("beginner_dont_block_buttons")?><BR>
        <button id="arbitral"><?=t("beginner_fill_with_arbitral")?></button></div>
</div>
<form id="file" class="step[0]" action="deserialize.php" method="POST">
    <input type="file" id="file" class="file">
    <button type="submit"><?=t("advanced_load_file")?></button>
</form>
<form id="base">
    <div  class="step[0]">
    <div id="untouchable">
    <input name="num_crit"><?=t("advanced_crit_size")?><BR>
    <input name="num_pref"><?=t("advanced_pref_size")?><BR>
    </div>
    </div>
</form>
<div class="form_func_hidden param">
    <div class="form long_form">
        <select class="class" name="class[]">
            <option value="Gauss">Gauss</option>
            <option value="Dirac">Dirac</option>
            <option value="Epanechnikov">Epanechnikov</option>
            <option value="Triangular">Triangular</option>
        </select>
        <input class="points" name="points[]" placeholder="<?=t("chart_points")?>">
        <input class="h" name="h[]" placeholder="<?=t("chart_smooth")?>">
        <input class="min" name="min[]" placeholder="<?=t("chart_min")?>">
        <input class="max" name="max[]" placeholder="<?=t("chart_max")?>">
        <input class="step" name="step[]" placeholder="<?=t("chart_step")?>">
        <button type="button" class="hide_button"><?=t("advanced_hide")?></button>
        <a download="chart.png" class="save_img"><?=t("advanced_save_chart")?></a>
    </div>
    <div class="short_form" style="display:none">
        <?=t("advanced_pdf")?>
    </div>
    <img style="position:absolute; display: none" src="web/assets/images/loading.gif">
</div>
<form id="matrices">
</form>
<form id="algorithms" class="step[2]">
<?php 
$algorithms = t("advanced_algorithms");
$desc = t("beginner_algorithm_descriptions");
foreach($classes as $type_id=>$cc) {
    $type_string = $algorithms[$type_id];
    $header = str_replace("\$", $type_string, t("advanced_choose_algorithm"));
    echo $header."<BR>\n";
    echo "<div class='select'>\n";
    echo "<select class='algorithm' name=\"algo[$type_id]\">\n";
    echo "<option value=\"\">".t("advanced_algorithm_none")."</option>\n";
    foreach($cc as $class) {
        $name = $class["name"];
        $explode = explode("\\", $name);
        $implode = implode(" ", $explode);
        $short_name = $explode[count($explode)-1];
        //$params = $class["params"];
        echo "<option value=\"$name\">$short_name</option>\n";
    }
    echo "</select><BR>\n";
    echo "<span class=\"tp\">".$desc[$type_id]."</span>\n";
    echo "</div>\n";
    foreach($cc as $class) {
        if(count($class["params"])>0) {
            $name = $class["name"];
            $explode = explode("\\", $name);
            $implode = implode(" ", $explode);
            $short_name = $explode[count($explode)-1];
            echo "<div class=\"$implode\" style=\"display:none\">\n";
            echo str_replace("\$", $short_name, t("advanced_set_params"))."<BR>\n";
            echo "<table border=1>"
            . "<tr><td>".t("advanced_param_name")."</td>\n"
            . "<td>".t("advanced_param_value")."</td></tr>\n";
            foreach($class["params"] as $param) {
                echo "<tr><td>$param</td>"
                . "<td><input name=\"params[".$name."][".$param."]\"></td></tr>\n";
            }
            echo "</table>\n";
            echo "</div>\n";
        }
    }
}
    ?>
</form>
<div class="step[5]">
        <button id="execute" type="button"><?=t("advanced_execute")?></button><img class="ajax_loading" src="web/assets/images/loading.gif">
</div>
<div id="results"></div>
<table class="footer">
    <tr>
        <td><button type="button" id="prev_step"><?=t("beginner_prev_step")?></button></td>
        <td><button type="button" id="prev_change"><?=t("beginner_revert_changes")?></button></td>
        <td><form id="serialize" action='serialize.php' method='POST' style="margin-bottom: 0">
            <input type='hidden' name='value'>
            <button disabled><?=t("advanced_serialize")?></button>
        </form></td>
        <td><button type="button" id="next_step"><?=t("beginner_next_step")?></button></td>
    </tr>
</table>
<script>
    $(".select").mousemove(function(e) {
        var span = $(this).find("span");
            var width = window.innerWidth;
            var height = window.innerHeight;
            if(e.pageX<width/2 && e.clientY<height/2) {
                span.offset({
                    top: e.pageY + 30,
                    left: e.pageX
                });
            } else if(e.pageX>=width/2 && e.clientY<height/2) {
                span.offset({
                    top: e.pageY + 30,
                    left: e.pageX - (span.outerWidth())
                });
            } else if(e.pageX<width/2 && e.clientY>=height/2) {
                span.offset({
                    top: e.pageY - (span.outerHeight()) - 20,
                    left: e.pageX
                });
            } else if(e.pageX>=width/2 && e.clientY>=height/2) {
                span.offset({
                    top: e.pageY  - (span.outerHeight()) - 20,
                    left: e.pageX - (span.outerWidth())
                });
            }
    });
</script>
<?php include("assets/scripts/ahp.js.php"); ?>
<?php include("assets/scripts/ahp_beginner.js.php"); ?>
<script src="web/assets/scripts/ahp-consistency.js"></script>
<script src="web/assets/scripts/ahp-dialog-generate-matrix.js"></script>