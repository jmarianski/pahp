<?php
$text = t("technical_description");
function parseText($array, $level) {
    echo "<h$level>".$array[0]."</h$level>";
    for($i=1; $i<count($array); $i++) {
        if(is_array($array[$i]))
            parseText ($array[$i], $level+1);
        else
            echo "<p>".$array[$i]."</p>";
    }
}
foreach($text as $line) {
    if(is_array($line))
        parseText($line,2);
    else
        echo "<p>$line</p>";
}