<?php
$app = app();
?>
<?php
if(!isset($title))
    $title = t("error");
?>
<!DOCTYPE html>
<html>
<head>
  <title><?=$title;?></title>
  <?php
  $files = glob("assets/styles/*.*");
  foreach($files as $f) {
      $url = "web/".$f;
      echo "<link href=\"$url\" rel=\"stylesheet\" type=\"text/css\" />";
  }
  ?>
</head>
<body>
    <div class="container">
    <!-- Header -->
    <div id="header">
      <h1><?=$title;?></h1>
    </div>

      <div id="content_container" class="alpha omega" style="margin-left: 0;">
        <div id="content" class="bBox">
          <?php echo $yield; ?>
            <a href="/pahp/index"><?=t("go_to_index")?></a>
        </div>
      </div>
    </div>

  <!-- JavaScripts -->
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $app->url('assets/scripts/bootstrap.min.js'); ?>"></script>
</body>
</html>
