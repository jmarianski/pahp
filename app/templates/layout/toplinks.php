<script>
$(function(){
    $('table.header td').hover(function(){
         $(this).children('ul').delay(20).slideDown(200);
    }, function(){
         $(this).children('ul').css("display", "none");
    });
    $('table.header li').click(function(){
         $(this).children('ul').each(function() {
             if($(this).css("display")=="none")
                $(this).delay(20).slideDown(200);
            else
                $(this).delay(20).slideUp(200);
         });
    });
});
</script>
    
<table class="header">
    <tr>
        <?php
        $urls = t("top_url");
        function url($u) {
            return "/pahp/".$u;
        }
        function recursive_urls($data) {
            foreach($data as $url=>$d) {
                if(is_array($d)) {
                    echo "<li class=\"has-submenu\">";
                    echo "<b><a href=\"".url($url)."\">".$d["name"]."</a></b>";
                    echo "<ul>";
                    recursive_urls($d["data"]);
                    echo "</ul>";
                } else {
                    echo "<li>";
                    echo "<a href=\"".url($url)."\">$d</a>";
                }
                echo "</li>";
            }
        }
        foreach($urls as $url=>$d) {
            echo "<td>";
            if(is_array($d)) {
                echo "<a href=\"".url($url)."\">".$d["name"]."</a>";
                echo "<ul>";
                recursive_urls($d["data"]);
                echo "</ul>";
            } else {
                echo "<a href=\"".url($url)."\">$d</a>";
            }
            echo "</td>";
        }
        ?>
    <td>
        <?php
            $langs = t("lang");
            ksort($langs);
            foreach($langs as $key=>$lang) {
                $img = "web/assets/images/lang/$key.png";
                echo "<a href=\"?lang=$key\" title=\"$lang\"><img src=\"$img\"></a>\n";
            }
        ?>
    </td>
     </tr>
 </table>
