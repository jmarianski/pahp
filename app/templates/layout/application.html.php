<?php
$app = app();
?>
<!DOCTYPE html>
<html>
<?php include('header.php'); ?>
<body>
    <div class="container">
        <?php include ("toplinks.php"); ?>
    <!-- Header -->
    <div id="header">
      <h1><?=$title;?></h1>
    </div>

      <div id="content_container" class="alpha omega" style="margin-left: 0;">
        <div id="content" class="bBox">
          <?php echo $yield; ?>
        </div>
      </div>
    </div>

  <!-- JavaScripts -->
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $app->url('web/assets/scripts/bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo $app->url('web/assets/scripts/main.js'); ?>"></script>
</body>
</html>
