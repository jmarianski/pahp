<?php
$app = app();
?>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<body>
  <?php
  $files = glob("assets/styles/*.*");
  foreach($files as $f) {
      $url = "web/".$f;
      echo "<link href=\"$url\" rel=\"stylesheet\" type=\"text/css\" />";
  }
  ?>
    <div class="container" style="width:99%;">
          <?php echo $yield; ?>
    </div>
</body>
</html>
