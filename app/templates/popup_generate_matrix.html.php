<style>
    input {
        width:100%;
        padding:0;
        margin:0;
    }
    td {
        padding-left:3px;
        padding-right:3px;
    }
</style>
<h1><?=t("popup_arbitral_header")?></h1>
    <?php
    if(!isset($n)) {
    echo t("popup_arbitral_doesnt_work_that_way");
    exit;
    }
    
    ?>
<?=t("popup_arbitral_description")?>
<table border="1" style="width:100%">
    <tr>
        <td><?=t("popup_arbitral_object")?></td>
        <td><?=t("popup_arbitral_arbitral_value")?></td>
        <td><?=t("popup_arbitral_ranking_value")?></td>
    </tr>
    <?php
    for($i=0; $i<$n; $i++) {?>
    <tr>
        <td><?=t("popup_arbitral_object")?> <?=($i+1)?></td>
        <td><input class="arbitral" tabindex="<?=($i+1)?>"></td>
        <td><input class="ranking" tabindex="<?=($i+$n+1)?>"></td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td>
            <input type="checkbox"><?=t("popup_arbitral_radicalize")?>
        <input type="hidden" name="value"></td>
        <td><button id="convert" type="button"><?=t("popup_arbitral_convert")?></button></td>
        <td><button id="finish" type="button"><?=t("popup_arbitral_finish")?></button></td>
    </tr>
    <tr id="hidden" style="display:none">
        <td><button id="root" type="button"><?=t("popup_arbitral_root")?></button></td>
        <td><button id="square" type="button"><?=t("popup_arbitral_square")?></button></td>
        <td><button id="revert" type="button"><?=t("popup_arbitral_revert")?></button></td>
    </tr>
</table>
<?=t("popup_arbitral_radicalize_description")?>
<script>
    $("button#finish").click(function() {
        var vals = getRanking();
        var ser = JSON.stringify(vals);
        $("input[name='value']").val(ser);
        self.close();
    });    
    $("button#root").click(function() {
        var vals = getRanking();
        for(var i=0; i<vals.length; i++)
            vals[i] = Math.sqrt(vals[i]);
        fillRanking(vals);
        normalize();
    });    
    $("button#square").click(function() {
        var vals = getRanking();
        for(var i=0; i<vals.length; i++)
            vals[i] *= vals[i];
        fillRanking(vals);
        normalize();
    });    
    $("button#revert").click(function() {
        var vals = getRanking();
        for(var i=0; i<vals.length; i++)
            vals[i] = 1/vals[i];
        fillRanking(vals);
        normalize();
    });
    function getRanking() {
        var vals = [];
        $("input.ranking").each(function(index, object) {
            vals[index] = parseFloat($(object).val());
        });
        return vals;
    }
    function fillRanking(vals) {
        $("input.ranking").each(function(index, object) {
            $(object).val(vals[index]);
        });
    }
    function normalize() {
        var vals = [];
        var min = null;
        var max = null;
        $("input.ranking").each(function(index, object) {
            var val = parseFloat($(object).val());
            vals[index] = val;
            if(min==null || min>val)
                min = val;
            if(max==null || max<val)
                max = val;
        });
        if(min<0) {
            for(var i=0; i<vals.length; i++) {
                vals[i] -= 2*min;
            }
            max -= 2*min;
            min -= 2*min;
        }
        if(max/min>9 || $("input[type='checkbox']").prop("checked")) {
            var b = (max - 9*min)/8;
            for(var i=0; i<vals.length; i++) {
                vals[i] += b;
            }
        }
        var sum = 0;
        for(var i=0; i<vals.length; i++) {
            sum += parseFloat(vals[i]);
        }
        for(var i=0; i<vals.length; i++) {
            vals[i] /= sum;
        }
        fillRanking(vals);
    }
    $("button#convert").click(function() {
        var vals = [];
        $("input.arbitral").each(function(index, object) {
            vals[index] = parseFloat($(object).val());
        });
        $("input.ranking").each(function(index, object) {
            $(object).val(vals[index]);
        });
        $("#hidden").css("display", "");
        normalize();
    });    
</script>