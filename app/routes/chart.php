<?php
// Options from root URL (should expose all available user choices)
$app->path('chart', function($request) use($app) {
    $app->get(function($request) use($app) {
        $filter = new Filter();
        extract($filter->draw($request->params()));
        $class = "Math\DF\\".$class;
        $gauss = new $class($points, $h);
        $gauss->generateCache($min, $max, $step);
        $array = $gauss->getCache($method);
        $draw = new Draw();
        $draw->drawPoints($array);
    });
});

