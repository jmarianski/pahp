<?php
// Options from root URL (should expose all available user choices)
$app->path(array(null, '', '/', 'draw'), function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_draw"));
            return $app->template('draw', $data);
    });
});

