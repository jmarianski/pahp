<?php
// Options from root URL (should expose all available user choices)
$app->path(array(null, '', '/', 'index'), function($request) use($app) {
    $app->get(function($request) use($app) {
            return $app->template('index', compact('data'));
    });
});

