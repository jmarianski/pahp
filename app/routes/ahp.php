<?php
// Options from root URL (should expose all available user choices)
$app->path( 'ahp', function($request) use($app) {
    $app->post(function($request) use($app) {
        //extract($filter->draw($request->params()));
        $filter = new Filter();
        $post = $request->params();
        if(!isset($post["base"])) // most likely wrong request
            $post = json_decode($request->raw(), true);
        $crit = $post["base"]["num_crit"];
        $pref = $post["base"]["num_pref"];
        $names = $post["base"]["names"];
        session_write_close();
        try {
            if(!isset($post["base"])) {
                throw new \RuntimeException("There was an error with your request and it can't be executed. Here is the request:\n".$request->raw());
            }
            ini_set('max_execution_time', 600);
            extract($filter->extractAlgorithms($post["algorithms"]));
            $priorities = $filter->extractMatrix($post["matrices"]["matrix"][0], $crit);
            $preferences = array();
            for($i=1; $i<=$crit; $i++) {
                $preferences[] = $filter->extractMatrix($post["matrices"]["matrix"][$i], $pref);
            }
            if(!isset($solver))
                throw new \RuntimeException("No solver supplied.");
            if(!isset($extractor)) 
                $extractor = new AHP\Algorithms\Extractors\Universal();
            if(isset($names))
                $extractor->setParamNames($names);
            $ahp = new AHP();
            $ahp->setSolverAlgorithm($solver);
            $ahp->setPreferenceMatrices($preferences);
            $ahp->setPrioritesMatrix($priorities);
            if(isset($consistency))
                $ahp->setConsistencyAlgorithm ($consistency);
            if(isset($generator))
                $ahp->setMatrixGenerator ($generator);
            if(isset($normalizer))
                $ahp->setNormalizer ($normalizer);
            $ahp->setResultExtractorAlgorithm($extractor);
            $result = $ahp->solve();
            $result->show();
        } catch (\Exception $e) {
            echo " <pre>".t("exception_occured").":\n".$e->getMessage()."<BR>\n".$e->getTraceAsString()."</pre>";
        }
        session_start();
        exit;
    });
});
$app->path('beginner', function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_beginner"));
            $classes = get_declared_classes();
            $classesWeNeed = array(
                "AHP\Algorithms\Solver",
                "AHP\Algorithms\ConsistencyAlgorithm",
                "AHP\Algorithms\MatrixGenerator",
                "AHP\Algorithms\Normalizer",
                "AHP\Algorithms\ResultExtractor",
            );
            foreach($classes as $c) {
                $cl = new ReflectionClass($c);
                if(!$cl->isAbstract()) {
                    foreach($classesWeNeed as $key=>$c2) {
                        if(is_subclass_of($c, $c2)) {
                            $arr["name"] = $c;
                            $obj = new $c();
                            $arr["params"] = $obj->getParams();
                            $data["classes"][$key][] = $arr;
                        }
                    }
                }
            }
            ksort($data["classes"]);
            return $app->template('beginner', $data);
    });
});
$app->path('advanced', function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_advanced"));
            $classes = get_declared_classes();
            $classesWeNeed = array(
                "AHP\Algorithms\Solver",
                "AHP\Algorithms\ConsistencyAlgorithm",
                "AHP\Algorithms\MatrixGenerator",
                "AHP\Algorithms\Normalizer",
                "AHP\Algorithms\ResultExtractor",
            );
            foreach($classes as $c) {
                $cl = new ReflectionClass($c);
                if(!$cl->isAbstract()) {
                    foreach($classesWeNeed as $key=>$c2) {
                        if(is_subclass_of($c, $c2)) {
                            $arr["name"] = $c;
                            $obj = new $c();
                            $arr["params"] = $obj->getParams();
                            $data["classes"][$key][] = $arr;
                        }
                    }
                }
            }
            ksort($data["classes"]);
            return $app->template('advanced', $data);
    });
});
$app->path('popup_generate_matrix', function($request) use($app) {
    $app->get(function($request) use($app) {
        
        $post = $request->params();
            return $app->template('popup_generate_matrix', array("n"=>$post["n"]))
                    ->layout("popup");
    });
});
$app->path('popup_generate_fuzzy', function($request) use($app) {
    $app->get(function($request) use($app) {
        $get = $request->params();
            return $app->template('popup_generate_fuzzy', array("val"=>$get["val"]))
                    ->layout("popup");
    });
});

