<?php
$app->path(array('api'), function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_api"));
            $data["url"] = $app->url();
            $classes = get_declared_classes();
            $classesWeNeed = array(
                "AHP\Algorithms\Solver",
                "AHP\Algorithms\ConsistencyAlgorithm",
                "AHP\Algorithms\MatrixGenerator",
                "AHP\Algorithms\Normalizer",
                "AHP\Algorithms\ResultExtractor",
            );
            foreach($classes as $c) {
                $cl = new ReflectionClass($c);
                if(!$cl->isAbstract()) {
                    foreach($classesWeNeed as $key=>$c2) {
                        if(is_subclass_of($c, $c2)) {
                            $arr["name"] = $c;
                            $obj = new $c();
                            $arr["params"] = $obj->getParams();
                            $data["classes"][$key][] = $arr;
                        }
                    }
                }
            }
            ksort($data["classes"]);
            return $app->template('api', $data);
    });
});
$app->path(array('technical'), function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_technical"));
            return $app->template('technical', $data);
    });
});
$app->path(array('science'), function($request) use($app) {
    $app->get(function($request) use($app) {
            $data = array("title"=>t("title_science"));
            return $app->template('science', $data);
    });
});

