<?php
/**
 * This class contains methods to filter $_GET fields. Mostly useful for default
 * values. 
 */
class Filter {
    /**
     * Used for drawing and default values for propabilistic functions. With this,
     * on default it will generate CDF of values between -5 and 5 with one single
     * point at (0,0), step=0.01 and smoothing param being inversely proportional
     * to number of elements in points array. This gives relatively smooth chart,
     * depending on conditions.
     * @param array $array predefined variables
     * @return array filtered values
     */
    public function draw($array) {
        $method = $array["method"];
        if(!in_array($method, array("cdf", "pdf")))
            $method = "cdf";
        $class = $array["class"];
        if(!in_array($class, array("Gauss", "Dirac", "Epanechnikov", "Triangular")))
            $class = "Gauss";
        $max = $array["max"];
        if(!is_numeric($max))
            $max = 5;
        $min = $array["min"];
        if(!is_numeric($min))
            $min = -5;
        $step = $array["step"];
        if(!is_numeric($step))
            $step = 0.01;
        $points = explode(",",$array["points"]);
        foreach($points as $key=>$p)
            if(!is_numeric($p))
                unset($points[$key]);
        if(count($points)<1)
            $points = array(0);
        $h = $array["h"];
        if($h<=0)
            $h = sqrt (1/count($points));
        return get_defined_vars();
    }
    /**
     * Extracts matrix from $_GET value. in case of propability matrix (instead of regular matrix)
     * it will be redirected to draw method to extract params. 
     * @param array $matrix
     * @param int $size
     * @return \Math\SquareMatrix|\Math\FunctionMatrix Matrix extracted from array.
     */
    public function extractMatrix($matrix, $size) {
        if($matrix["type"]=="regular" || $matrix["type"]=="fuzzy") {
            $result = array();
            for($i=0; $i<$size; $i++) {
                $row = array();
                for($j=0; $j<$size; $j++) {
                    if(isset($matrix["data"][$i][$j]))
                        $row[] = $matrix["data"][$i][$j];
                    else
                        $row[] = 0;
                }
                $result[] = $row;
            }
            if($matrix["type"]=="regular")
                return new \Math\SquareMatrix($result);
            else if($matrix["type"]=="fuzzy")
                return new \Math\FuzzyMatrix($result);
        } else if($matrix["type"]=="func") {
            $result = array();
            for($i=0; $i<$size; $i++) {
                $params = $this->draw($matrix["data"][$i]);
                $df = new \Math\DF\Gauss($params["points"], $params["h"]);
                $df->generateCache($params["min"], $params["max"], $params["step"]);
                $result[] = $df;
            }
            return new Math\FunctionMatrix($result);
        }
    }
    /**
     * Extracts algorithms and params for those algorithms. If they are specified, it will
     * set those params automatically. It will crash on improper value names.
     * @param type $array
     * @return array|\class
     */
    public function extractAlgorithms($array) {
        //array("Solver", "Consistency", "Matrix Generator", "Normalizer", "Result Extractor"),
        $result = array();
        $vars = array("solver", "consistency", "generator", "normalizer", "extractor");
        if(!is_array($array))
            return $result;
        foreach($vars as $key=>$val) {
            if(isset($array["algo"][$key]) && $array["algo"][$key]!=false) {
                $class = $array["algo"][$key];
                $result[$val] = new $class();
                if(isset($array["params"]) && is_array($array["params"]) && is_array($array["params"][$class])) {
                    $result[$val]->setParams($array["params"][$class]);
                }
            }
        }
        return $result;
    }
}
