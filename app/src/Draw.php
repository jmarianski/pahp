<?php
/**
 * This class has only one purpose: to use pChart to draw charts. Right now - as
 * of writing this doc - there is one method: drawPoints, that accepts array of
 * keys and values, where keys get to abscissa, and values get to ordinate.
 */
Class Draw{
    /**
     * Draws points, where keys of the array land on abscissa, and values to
     * ordinate.
     * @param type $array
     */
    public function drawPoints($array) {
        session_write_close();
         /* CAT:Line chart */
        $keys = array_keys($array);
        $values = array_values($array);
         /* Set the default timezone */
         date_default_timezone_set('Etc/GMT');
         $x = count($keys);

         /* Create and populate the pData object */
         $MyData = new pData();

           $MyData->addPoints($values,"Y");
           $MyData->addPoints($keys,"X");
         $MyData->setAbscissa("X");
        $MyData->setPalette("Y",array("R"=>0, "G"=>0, "B"=>0));

        /* Create the pChart object */
        $myPicture = new pImage(700,230,$MyData);

        /* Add a border to the picture */
        $myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));

        /* Set the default font */
        $myPicture->setFontProperties(array("FontName"=>"../pChart2.1.4/fonts/pf_arma_five.ttf","FontSize"=>9));

        /* Define the chart area */
        $myPicture->setGraphArea(60,40,650,200);

        /* Draw the scale */
        $scaleSettings = array("XMargin"=>0,"YMargin"=>0,"Floating"=>TRUE,"GridR"=>230,"GridG"=>230,"GridB"=>230,"DrawSubTicks"=>FALSE,
            "DrawXLines"=>true, "Mode"=>SCALE_MODE_FLOATING);
        if($x>20)
            $scaleSettings["LabelSkip"] = ($x-11)/11;
        $myPicture->drawScale($scaleSettings);

        /* Turn on Antialiasing */
        $myPicture->Antialias = TRUE;

        /* Draw the line chart */
        $myPicture->drawLineChart();

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput("pictures/example.drawLineChart.simple.png"); 
        session_start();
    }

}
