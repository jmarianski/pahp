<?php

namespace Math;
/**
 * Set of functions that operate on eigen vectors.
 */
class EigenVector {
    private $ri_func;
    private $matrix;
    private $vector;
    private $consistency;
    /**
     * Sets matrix for calculations
     * @param \Math\AbstractMatrix $matrix
     */
    public function setMatrix(AbstractMatrix $matrix) {
        $this->matrix = $matrix;
    }
    /**
     * Allows you to set custom RI function.
     * @param type $func
     */
    public function setRIFunction($func) {
        $this->ri_func = $func;
    }
    /**
     * Default RI function
     * @param int $n
     * @return real
     */
    public function RI($n) {
        if($n<3)
            return 0.000001;
        else if ($n<=10)
            switch($n) {
                case 3: return 0.58;
                case 4: return 0.9;
                case 5: return 1.12;
                case 6: return 1.24;
                case 7: return 1.32;
                case 8: return 1.41;
                case 9: return 1.45;
                case 10: return 1.49;
            }
        else return 1.49;
    }
    
    /**
     * Calculates consistency and eigen vector based on supplied matrix.
     * @return array [vector, consistency]
     * @throws \RuntimeException
     */
    public function calculate() {
        if($this->ri_func==null)
            $this->ri_func = array($this, "RI");
        $n = $this->matrix->num_rows();
        $vector = Matrix::zeroes($n, 1);
        $sums = Matrix::zeroes(1, $n);
        for($j=0; $j<$n; $j++) {
            $sum = 0;
            for($i=0; $i<$n; $i++) {
                $val = $this->matrix->get($i, $j);
                if($val<=0)
                    throw new \RuntimeException("EigenVector error: couldn't calculate, because matrix has zeroes (or negative values).");
                $sum += $this->matrix->get($i, $j);
            }
            $sums->set(0, $j, $sum);
            for($i=0; $i<$n; $i++) {
                $vector->add($i, 0, $this->matrix->get($i, $j)/$n/$sum);
            }
        }
        $lambdaMatrix = Matrix::multiply($sums, $vector);
        $lambda = $lambdaMatrix->get(0,0);
        if($n<3)
            $this->consistency = 0;
        else {
            $CI = ($lambda - $n)/($n-1);
            $RI = call_user_func($this->ri_func, $n);
            $this->consistency = $CI/$RI;
        }
        $this->vector = $vector;
        return array($this->vector, $this->consistency);
    }
}
