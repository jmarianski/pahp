<?php


namespace Math\DF;

class Dirac extends DistributionFunction {

    public function cdfFunction($x) {
        $less = 0;
        foreach($this->points as $p){
            if($p<$x) $less++;
        }
        return $less/count($this->points);
    }

    public function pdfFunction($x) {
        if(abs($x)>0.0000001) return 0;
        return 1/0.0000002;
    }
    
    
    public function getRandom($from, $to){
        return $this->points[mt_rand(0, count($this->points) - 1)];
    }
    public function getRandomCache(){
        return $this->points[mt_rand(0, count($this->points) - 1)];
    }

}
