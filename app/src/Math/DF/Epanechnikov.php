<?php


namespace Math\DF;

class Epanechnikov extends DistributionFunction {

    public function cdfFunction($x) {
        if($x<-1) return 0;
        if($x>1) return 1;
        return (3.0/4.0)*($x-$x*$x*$x/3.0)+1.0/2.0;
    }

    public function pdfFunction($x) {
        if(abs($x)>1) return 0;
        return (1-$x*$x)*3/4;
    }

}
