<?php


namespace Math\DF;

class Triangular extends DistributionFunction {

    public function cdfFunction($x) {
        if($x<-1) return 0;
        if($x>1) return 1;
        if($x<0){
            return $x+$x*$x/2+1/2;
        }
        if($x>=0) {
            return $x - $x*$x/2 + 1/2;
        }
    }

    public function pdfFunction($x) {
        if(abs($x)>1) return 0;
        return 1-abs($x);
    }

}
