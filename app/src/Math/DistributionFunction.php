<?php

namespace Math\DF;
/**
 * Data backing a propability function. To use it efficiently user should declare required
 * parameters, create cache of data, and then using it. That way random values get generated quicker
 * and charts get printed faster.
 */
abstract class DistributionFunction {
    protected $points;
    private $cdfCache;
    private $pdfCache;
    private $cacheMin;
    private $cacheMax;
    private $h;
    private $randomError = 0.02;
    /**
     * When you subclass this Function, you need to specify CDF and PDF for any given point.
     * This is CDF - Cumultative Distribution Function. It provides information, how
     * many values below this point are propable with this function.
     */
    public abstract function cdfFunction($x);
     /**
     * When you subclass this Function, you need to specify CDF and PDF for any given point.
     * This is PDF - Propability Density Function. Provides information what is the likelihood
     * of getting variable at this point. Function must be constructed in a way, that its
      * integral (from -INF to +INF) is equal to 1.
     */
    public abstract function pdfFunction($x);
    
    public function __construct(Array $points, $h=1) {
        $this->points = $points;
        $this->cdfCache = array();
        $this->pdfCache = array();
        $this->h = $h;
    }
    /**
     * Calculates pdf for these points based on pdfFunction and smoothing param.
     * @param type $x
     * @return type
     */
    public function pdf($x){
        $n = count($this->points);
        $sum=0;

        foreach($this->points as $p){
            $sum+=$this->pdfFunction(($x-$p)/$this->h);
        }
        $sum /= ($n*$this->h);
        return $sum;
    }
    
    
    /**
     * Calculates cdf for these points based on cdfFunction and smoothing param.
     * @param type $x
     * @return type
     */
    public function cdf($x){
        $n = count($this->points);
        $sum=0;

        foreach($this->points as $p){
            $sum+=$this->cdfFunction(($x-$p)/$this->h);
        }
        $sum = $sum/($n);
        return $sum;
    }
    /**
     * Checks if cache was generated. It usually is, when any of the caches isn't empty.
     * @return bool
     */
    public function isCacheGenerated() {
        return count($this->cdfCache)>0;
    }
    /**
     * Gets cache for specified function.
     * @param type $func
     * @return type
     */
    public function getCache($func) {
        if($func=="cdf")
            return $this->cdfCache;
        if($func=="pdf")
            return $this->pdfCache;
    }
    /**
     * Gets CDF from cache. If it wasn't calculated, generates it.
     * @param float $x
     * @return float
     */
    public function getCdfCache($x){
        $x = (string)$x;
        $cached = $this->cdfCache[$x];
        if($cached!==null){
            return $cached;
        }
        return $this->cdf($x);
    }
    /**
     * Gets PDF from cache. If it wasn't calculated, generates it.
     * @param float $x
     * @return float
     */
    public function getpdfCache($x){
        $x = (string)$x;
        $cached = $this->pdfCache[$x];
        if($cached!==null){
            return $cached;
        }
        return $this->pdf($x);
    }
    /**
     * If we want to specify random value to be with greater accuracy (which is not recommended)
     * this function will allow you to set it.
     * @param float $float
     */
    public function setError($float) {
        $this->randomError = (float) $float;
    }
    /**
     * Generates cache with specified params
     * @param float $from Minimum point of function
     * @param float $to Maximum point of function
     * @param float $step What is going to be a difference between arguments in array.
     * @throws \RuntimeException
     */
    public function generateCache($from, $to, $step) {
        if($to<=$from || $step<=0)
            throw new \RuntimeException("Wrong values");
        $this->cacheMin = $from;
        $this->cacheMax = $to;
        $n = (int)(($to-$from)/$step);
        for($i=0; $i<$n; $i++) {
            $x = $from + $step*$i;
            $this->cdfCache[(string)$x] = $this->cdf($x);
            $this->pdfCache[(string)$x] = $this->pdf($x);
        }
    }
    /**
     * Based on cache returns closest value to generated random
     */
    public function getRandomCache() {
        if($this->cacheMin == null || $this->cacheMax == null || $this->randomError <=0)
            throw new \RuntimeException("First: fill cache (and error)");
        $float = $this->randomFloat(0, 1);
        $keys = array_keys($this->cdfCache);
        $b1 = 0;
        $b2 = count($keys)-1;
        for($i=0;$i<1000;$i++){
            $mid = (int)(($b1+$b2)/2);
            $val = $this->cdfCache[$keys[$mid]];
            if(abs($val-$float)<$this->randomError) break;
            if($val>$float) $b2 = $mid;
            else $b1 = $mid;
        }
        // echo "error: ".abs(abs($val-$float)-$this->randomError)."<BR>";
        return $keys[$mid];
    }
    /**
     * Conventional random generator. Will search desired fragment of space for random
     * value on CDF.
     * @param float $from
     * @param float $to
     * @return float
     * @throws \RuntimeException
     */
    public function getRandom($from, $to) {
        if($this->randomError <=0 || $to<=$from)
            throw new \RuntimeException("Errors in get random");
        $float = $this->randomFloat(0, 1);
        for($i=0;$i<1000;$i++){
            $mid = (int)(($from+$to)/2);
            $val = $this->cdf($mid);
            if(abs($val-$float)<$this->randomError) break;
            if($val>$float) $to = $mid;
            else $from = $mid;
        }
        // echo "error: ".abs(abs($val-$float)-$this->randomError)."<BR>";
        return $mid;
    }
    /**
     * Generates random value between points.
     * @param type $min
     * @param type $max
     * @return type
     */
    private function randomFloat($min,$max) {
        return ($min+lcg_value()*(abs($max-$min)));
    }
    /**
     * Prints out very simple textual representation of this Function. 
     * @return string
     */
    public function toString() {
        return get_class($this)." (".implode(", ", $this->points)."), h=".$this->h;
    }
    
}
