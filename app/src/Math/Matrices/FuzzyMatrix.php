<?php


namespace Math;

class FuzzyMatrix extends AbstractMatrix {
    /**
     * Square matrix must have equal number of rows and columns.
     * @param array $array
     * @return mixed
     * @throws \RuntimeException
     */
    public function validate(Array &$array) {
        $arr = parent::validate($array);
        if($arr[0]!=$arr[1])
            throw new \RuntimeException("Matrix initialization failed: Not a square matrix.");
        for($i=0; $i<$arr[0]; $i++) {
            for($j=0; $j<$arr[1]; $j++) {
                if(is_string($array[$i][$j])) {
                    try { //assuming faulty records as empty ones
                        $array[$i][$j] = new FuzzyValue($array[$i][$j]);
                    } catch (\Exception $e) {
                        $array[$i][$j] = null;
                    }
                } else if(is_array($array[$i][$j]))
                    $array[$i][$j] = new FuzzyValue(json_encode($array[$i][$j]));
                else if(!($array[$i][$j] instanceof FuzzyValue))
                    $array[$i][$j] = null;
            }
        }
        return $arr;
    }
    /**
     * Factory method to generate matrix filled with zeroes.
     * @param type $n
     * @param type $_
     * @return \Math\SquareMatrix
     */
    public static function zeroes($n) {
        return new FuzzyMatrix(self::fillArray($n, $n, null));
    }
    /**
     * Factory method to generate matrix filled with ones.
     * @param type $n
     * @return \Math\SquareMatrix
     */
    protected static function ones($n) {
        return new FuzzyMatrix(self::fillArray($n, $n, "[[1,1,1]]"));
    }
    
    
    /**
     * Shows this matrix. Filters values with number format to avoid more than 3
     * decimal points.
     */
    public function show(){
        echo "<table border='1'>";
        if($this->params!=null) {
            echo "<tr><td></td>";
            for($i=0;$i<$this->rows;$i++){
                echo "<td>".$this->params[$i]."</td>";
            }
            echo "</tr>";
        }
        for($i=0;$i<$this->rows;$i++){
            echo "<tr>";
            if($this->params!=null && $this->params[$i]!=null) {
                echo "<td>".$this->params[$i]."</td>";
            }
            for($j=0;$j<$this->columns;$j++){
                if($this->matrix[$i][$j]!=null)
                    echo "<td>".($this->matrix[$i][$j]->show())."</td>";
                else
                    echo "<td>null</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    
    public function calculateEigenVector() {
        $n = $this->num_rows();
        $sums = array();
        $sum_all = FuzzyValue::zeroes();
        for($i=0; $i<$n; $i++) {
            $sums[$i] = FuzzyValue::zeroes();
            for($j=0; $j<$n; $j++) {
                $val = $this->get($i, $j); //FuzzyValue
                if($val==null)
                    throw new \RuntimeException("EigenVector error: couldn't calculate, because matrix has zeroes (or negative values).");
                $sums[$i]->add($val);
                $sum_all->add($val);
            }
        }
        $sum_all->inverse();
        for($i=0; $i<$n; $i++) {
            $sums[$i]->multiply($sum_all);
        } // S1-SN generated
        $V = array();
        for($i=0;$i<$n;$i++) {
            for($j=0; $j<$n; $j++) {
                // V[i] >= V[j] <=> m[i] >= m[j]
                if($sums[$i]->average(1)>=$sums[$j]->average(1))
                    $V[$i][$j] = 1; 
                else { 
                    /* From Da-Yong Chang
                     * M2 >= M1 <=>
                     * l1-u2 / (m2 - u2) - (m1 - l1) <=>
                     * l1 - u2 / m2- m1 + l1 - u2
                     * Then
                     * M0 >= M1 <=>
                     * l1-u0 / m0 - m1 + l1 - u0
                     */
                    $u0 = $sums[$i]->average(2);
                    $l1 = $sums[$j]->average(0);
                    $m0 = $sums[$i]->average(1);
                    $m1 = $sums[$j]->average(1);
                    $V[$i][$j] = ($l1 - $u0)/($m0 - $m1 + $l1 - $u0);
                    if($V[$i][$j]<0)
                        $V[$i][$j] = 0;
                }
                
            }
        }
        $w = array();
        $sum = 0;
        for($i=0;$i<$n;$i++) {
            $min = 1;
            for($j=0;$j<$n; $j++) {
                if($V[$i][$j]<$min)
                    $min = $V[$i][$j];
            }
            $w[$i] = array($min);
            $sum += $min;
        }
        // normalizing
        for($i=0; $i<$n; $i++)
            $w[$i][0] /= $sum;
        $vector = new \Math\Matrix($w);
        return array($vector, 0);
    }
}
