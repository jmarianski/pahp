<?php


namespace Math;
/**
 * Square matrix has just one requirement: it's number of rows must be equal to columns.
 */
class SquareMatrix extends Matrix {
    /**
     * Square matrix must have equal number of rows and columns.
     * @param array $array
     * @return mixed
     * @throws \RuntimeException
     */
    public function validate(Array &$array) {
        $arr = parent::validate($array);
        if($arr[0]!=$arr[1])
            throw new \RuntimeException("Matrix initialization failed: Not a quadratic matrix.");
        return $arr;
    }
    /**
     * Factory method to generate matrix filled with zeroes.
     * @param type $n
     * @param type $_
     * @return \Math\SquareMatrix
     */
    public static function zeroes($n, $_ = null) {
        return new SquareMatrix(self::fillArray($n, $n, 0));
    }
    /**
     * Factory method to generate matrix filled with ones.
     * @param type $n
     * @return \Math\SquareMatrix
     */
    protected static function ones($n) {
        return new SquareMatrix(self::fillArray($n, $n, 1));
    }
    
    
    /**
     * Shows this matrix. Filters values with number format to avoid more than 3
     * decimal points.
     */
    public function show(){
        echo "<table border='1'>";
        if($this->params!=null) {
            echo "<tr><td></td>";
            for($i=0;$i<$this->rows;$i++){
                echo "<td>".$this->params[$i]."</td>";
            }
            echo "</tr>";
        }
        for($i=0;$i<$this->rows;$i++){
            echo "<tr>";
            if($this->params!=null && $this->params[$i]!=null) {
                echo "<td>".$this->params[$i]."</td>";
            }
            for($j=0;$j<$this->columns;$j++){
                echo "<td>".number_format($this->matrix[$i][$j], 3)."</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    
    public function calculateEigenVector() {
        $n = $this->num_rows();
        $vector = Matrix::zeroes($n, 1);
        $sums = Matrix::zeroes(1, $n);
        for($j=0; $j<$n; $j++) {
            $sum = 0;
            for($i=0; $i<$n; $i++) {
                $val = $this->get($i, $j);
                if($val<=0)
                    throw new \RuntimeException("EigenVector error: couldn't calculate, because matrix has zeroes (or negative values).");
                $sum += $this->get($i, $j);
            }
            $sums->set(0, $j, $sum);
            for($i=0; $i<$n; $i++) {
                $vector->add($i, 0, $this->get($i, $j)/$n/$sum);
            }
        }
        $lambdaMatrix = Matrix::multiply($sums, $vector);
        $lambda = $lambdaMatrix->get(0,0);
        if($n<3)
            $consistency = 0;
        else {
            $CI = ($lambda - $n)/($n-1);
            $RI = $this->RI($n);
            $consistency = $CI/$RI;
        }
        return array($vector, $consistency);
    }
}
