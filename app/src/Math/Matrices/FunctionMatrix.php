<?php

namespace Math;
/**
 * Implementation of Matrix, that in essence is an EigenVector with values specified
 * as propable values. It means, that if - in relation to other values - one function
 * is represented as gauss function (with specified params), the other must also be represented
 * with propability function. If we'd like to use one value, simply use Dirac Function.
 */
class FunctionMatrix extends AbstractMatrix {
    /**
     * We need special validation: all elements of supplied array must be Propability 
     * Functions.
     * @param array $array
     * @return type
     * @throws \RuntimeException
     */
    public function validate(Array &$array) {
        $r = false;
        $r = count($array);
        if($r==0)
            throw new \RuntimeException("Matrix initialization failed: Not a matrix: Number of rows is 0");
        foreach($array as $rownum=>$value) {
            if(!($value instanceof DF\DistributionFunction))
                throw new \RuntimeException("Matrix initialization failed: Not a matrix: Element $rownum is not a distribution function");
        }
        return array($r, 1);
    }
    /**
     * Unusable: this function should not be used.
     */
    public function set($x, $y, $value) {
        throw new \RuntimeException("Can't use this method.");
    }
    /**
     * Number of elements.
     * @return type
     */
    public function size() {
        return $this->num_rows();
    }
    /**
     * Gets element at x.
     * @param type $x
     * @param type $_
     * @return type
     */
    public function get($x, $_ = null) {
        return $this->matrix[$x];
    }

    public function num_columns() {
        return 1;
    }

    public function num_rows() {
        return $this->rows;
    }
    
    public function getArray() {
        return $this->matrix;
    }
    
    public function show(){
        echo "<table border='1'>";
        for($i=0;$i<$this->rows;$i++){
            echo "<tr><td>";
            echo $this->get($i)->toString();
            echo "</td></tr>";
        }
        echo "</table>";
    }

    public function calculateEigenVector() {
        $size = $this->size();
        $gen = array();
        for($j=0; $j<$size; $j++) {
            $gen[$j] = array();
            $DF = $this->get($j,0);
            if(!$DF->isCacheGenerated())
                $DF->generateCache(-5, 15, 0.01);
            $gen[$j][0] = $DF->getRandomCache();
        }
        $m = new \Math\Matrix($gen);
        if(isset($this->normalizer))
            $this->normalizer->noramalizeEigenVector($m);
        return array($m, 0);
    }

}
