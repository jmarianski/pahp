<?php
namespace Math;

class Matrix extends AbstractMatrix {
    /**
     * We need special validation: we must specify an array that creates a rectangle.
     * @param array $array
     * @return type
     * @throws \RuntimeException
     */
    public function validate(Array &$array) {
        $r = $c = false;
        $r = count($array);
        if($r==0)
            throw new \RuntimeException("Matrix initialization failed: Not a matrix: Number of rows is 0");
        foreach($array as $rownum=>$row) {
            $cols = count($row);
            if($c==false)
                $c = $cols;
            else if($c==0)
                throw new \RuntimeException("Matrix initialization failed: Not a matrix: Number of columns is 0 (row: $rownum)");
            else if($cols!==$c)
                throw new \RuntimeException("Matrix initialization failed: Not a matrix: Inconsistent number of rows (1st row: $c; $rownum:$cols)");
            else if(!is_array($row))
                throw new \RuntimeException("Not a matrix: has rows but not columns. ($rownum)");
            foreach($row as $column=>$value) {
                if(is_array($value))
                    throw new \RuntimeException("Matrix initialization failed: Not a 2D matrix: Element $rownum:$column is an array.");
                if(!is_numeric($value))
                    throw new \RuntimeException("Matrix initialization failed: Not a matrix: Element $rownum:$column is not a numeric");
            }
        }
        return array($r, $c);
    }
    
    /**
     * Factory method: generates empty Matrix
     * @param int $n
     * @param int $m
     * @return \Math\Matrix
     * @throws \RuntimeException
     */
    public static function zeroes($n, $m) {
        return new Matrix(self::fillArray($n, $m, 0));
    }
    /** Concatenates many straight vertical lines into one matrix */
    public static function concatenateLines(Array $a) {
        $columns = count($a);
        $rows = $a[0]->num_rows();
        $newMatrix = self::zeroes($rows, $columns);
        foreach($a as $index=>$matrix) {
            if(!($matrix instanceof AbstractMatrix) || $matrix->num_columns()>1 ||  $matrix->num_rows()!=$rows)
                throw new \RuntimeException("There was an error concatenating matrices. There was something wrong with them.\nFirst matrix:".print_r($a[0], true)
                        ."\nMatrix in question:".print_r($matrix, true));
            for($i=0; $i<$rows; $i++) {
                $newMatrix->set($i, $index, $matrix->get($i, 0));
            }
        }
        return $newMatrix;
    }
    /**
     * Multiplies matrices.
     * @param \Math\Matrix $A
     * @param \Math\matrix $B
     * @return type
     * @throws \RuntimeException Thrown when matrices are incompatible.
     */
    public static function multiply(Matrix $A, Matrix $B) {
        if($A->num_columns()!=$B->num_rows())
            throw new \RuntimeException("Matrix multiplication error: Incompatible matrices: A = ".$A->num_columns()."X".$A->num_rows().
                    ", B = ".$B->num_columns()."X".$B->num_rows());
        $a = $A->num_rows();
        $b = $B->num_columns();
        $len = $A->num_columns(); // $A->num_columns() == $B->num_rows()
        $result = self::zeroes($a, $b);
        for($i=0; $i<$a; $i++) {
            for($j=0; $j<$b; $j++) {
                for($x=0; $x<$len; $x++) {
                    $result->add($i, $j, $A->get($i, $x)*$B->get($x, $j));
                }
            }
        }
        return $result;
    }
    /**
     * Different type of "set": we add value to the one already in the matrix.
     * @param type $x
     * @param type $y
     * @param type $value
     * @throws \RuntimeException
     */
    public function add($x, $y, $value) {
        if(!is_numeric($value))
            throw new \RuntimeException("Wrong argument: must be a numeric");
        $this->matrix[$x][$y] += $value;
    }

    public function calculateEigenVector() {
        return false;
    }

}
