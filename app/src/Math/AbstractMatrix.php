<?php

namespace Math;
/**
 * This class defines basic methods that apply to any kind of matrix, be it vector,
 * square matrix or other things. Defines some required methods.
 */
abstract class AbstractMatrix {
    
    public $matrix = array(array());
    protected $columns;
    protected $rows;
    protected $params;
    /** Creates a matrix based on this array. Must be a valid matrix array (consistent
     * number of rows and columns for each element). Only 2D matrices are accepted.
     * @param array $array Basis for this matrix
     * @throws \RuntimeException In case of inconsistencies.
     */
    public function __construct(Array $array) {
        list($r, $c) = $this->validate($array);
        $this->matrix = $array;
        $this->columns = $c;
        $this->rows = $r;
    }
    /**
     * Validates if array can be used as matrix. Used in constructor to ensure
     * that everythings is OK.
     * @param array $array
     * @return array rows and columns count.
     * @throws \RuntimeException
     */
    public function validate(Array &$array) {
        $r = $c = false;
        $r = count($array);
        if($r==0)
            throw new \RuntimeException("Matrix initialization failed: Not a matrix: Number of rows is 0");
        foreach($array as $rownum=>$row) {
            $cols = count($row);
            if($c==false)
                $c = $cols;
            else if($c==0)
                throw new \RuntimeException("Matrix initialization failed: Not a matrix: Number of columns is 0 (row: $rownum)");
            else if($cols!==$c)
                throw new \RuntimeException("Matrix initialization failed: Not a matrix: Inconsistent number of rows (1st row: $c; $rownum:$cols)");
            else if(!is_array($row))
                throw new \RuntimeException("Not a matrix: has rows but not columns. ($rownum)");
            foreach($row as $column=>$value) {
                if(is_array($value))
                    throw new \RuntimeException("Matrix initialization failed: Not a 2D matrix: Element $rownum:$column is an array.");
            }
        }
        return array($r, $c);
    }
    /**
     * Helper static method. Fills array with values so that it creates valid matrix filled
     * with exactly one value.
     * @param int $n
     * @param int $m
     * @param mixed $value any value, you wish to fill the matrix with.
     * @return array
     * @throws \RuntimeException
     */
    protected static function fillArray($n, $m, $value) {
        if($n<1 || $m<1)
            throw new \RuntimeException("Matrix initialization failed: Wrong dimensions.");
        $result = array();
        for($i=0; $i<$n; $i++) {
            $result[$i] = array_fill(0, $m, $value);
        }
        return $result;
    }
    /**
     * Set value at point x,y.
     * @param int $x
     * @param int $y
     * @param mixed $value
     */
    public function set($x, $y, $value) {
        $this->matrix[$x][$y] = $value;
    }
    /**
     * Gets value from point x,y
     * @param int $x
     * @param int $y
     * @return mixed
     */
    public function get($x, $y) {
        return $this->matrix[$x][$y];
    }
    /**
     * Returns number of columns
     * @return int
     */
    public function num_columns() {
        return $this->columns;
    }
    /**
     * Returns number of rows
     * @return int
     */
    public function num_rows() {
        return $this->rows;
    }
    /**
     * Returns array backing this matrix.
     * @return type
     */
    public function getArray() {
        return $this->matrix;
    }
    /**
     * Shows this matrix. Filters values with number format to avoid more than 3
     * decimal points.
     */
    public function show(){
        echo "<table border='1'>";
        for($i=0;$i<$this->rows;$i++){
            echo "<tr>";
            if($this->params!=null && $this->params[$i]!=null) {
                echo "<td>".$this->params[$i]."</td>";
            }
            else
                echo "<td></td>";
            for($j=0;$j<$this->columns;$j++){
                echo "<td>".number_format($this->matrix[$i][$j], 3)."</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    public function setParamNames($array) {
        $this->params = $array;
    }
    
    public abstract function calculateEigenVector();
    
        /**
     * Default RI function
     * @param int $n
     * @return real
     */
    protected function RI($n) {
        if($n<3)
            return 0.000001;
        else if ($n<=10)
            switch($n) {
                case 3: return 0.58;
                case 4: return 0.9;
                case 5: return 1.12;
                case 6: return 1.24;
                case 7: return 1.32;
                case 8: return 1.41;
                case 9: return 1.45;
                case 10: return 1.49;
            }
        else return 1.49;
    }
}
