<?php
namespace Math;

class FuzzyValue {
    public $data;
    public function __construct($string) {
        //check if we've supplied array
        if(is_array($string))
            $array = $string;
        else
            $array = json_decode($string, true);
        $i = 0;
        foreach($array as $key=>$value) {
            if($key!=$i++ || !is_array($value) || count($value)!=3)
                throw new \Exception("wrong data type");
            $j = 0;
            foreach($value as $key2=>$value2) {
                if($key2!=$j++ 
                        || !is_numeric($value2) 
                        || $value2<0 
                        || ($key2>0 && $value[$key2-1]>$value2)) // previous elem is bigger than this
                    throw new \Exception("wrong data type");
                else
                    $array[$key][$key2] = (float) $value2;
            }
        }
        $this->data = $array;
    }
    
    public static function zeroes() {
        return new FuzzyValue(array(array(0,0,0)));
    }
    
    private function get_average() {
        if(count($this->data)==1)
            return $this->data;
        $result = array(array(0,0,0));
        $n = count($this->data);
        foreach($this->data as $row) {
            for($i=0; $i<3; $i++)
                $result[0][$i] += $row[$i]/$n; //error margin should be minimal
        }
        $this->data = $result;
        return $result;
    }
    
    public function inverse() {
        $new_rows = array();
        foreach($this->data as $row) {
            $new_row = array();
            for($i=0; $i<3; $i++)
                $new_row[$i] = 1/$row[2-$i];
            $new_rows[] = $new_row;
        }
        $this->data = $new_rows;
        return $this->data;
    }
    
    public function add(FuzzyValue $value) {
        if(count($this->data)>1)
            $this->average(); // convert to 1-row
        $avg = $value->average();
        for($i=0; $i<3; $i++)
            $this->data[0][$i] += $avg[0][$i];
        return $this->data;
    }
    
    public function average($i = -1) {
        $avg = $this->get_average();
        if($i>=0 && $i<=3) {
            return $avg[0][$i];
        } else
            return $avg;
    }
    
    public function multiply(FuzzyValue $value) {
        if(count($this->data)>1)
            $this->average(); // convert to 1-row
        $avg = $value->average();
        for($i=0;$i<3;$i++) {
            $this->data[0][$i] *= $avg[0][$i];
        }
        return $this->data;
    }
    
    public function show() {
        return json_encode($this->data);
    }
}
