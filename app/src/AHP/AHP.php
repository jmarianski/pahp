<?php
/**
 * Main class of this whole program. Accepts algorithms and allows interactions
 * between them. Most importantly, has "solve" method that sparks interactions
 * between algorithms. 
 */
class AHP{

    /** @var \Math\SquareMatrix $criterias */
    private $criterias;
    /** @var Array $preferences */
    private $preferences;
    /** @var \AHP\Algorithms\ConsistencyAlgorithm $criterias */
    private $consistency;
    /** @var \AHP\Algorithms\Solver $criterias */
    private $solver;
    /** @var \AHP\Algorithms\ResultExtractor $criterias */
    private $extractor;
    /** @var \AHP\Algorithms\MatrixGenerator $criterias */
    private $generator;
    /** @var \AHP\Algorithms\Normalizer $normalizer */
    private $normalizer;


    public function __construct(){

    }
    /**
     * Sets priority matrix (criterias). Has to be the same size as number of matrices provided in
     * setPreferanceMatrices method.
     * @param \Math\SquareMatrix $matrix
     */
    public function setPrioritesMatrix(\Math\AbstractMatrix $matrix) {
        $this->criterias = $matrix;
    }
    /**
     * Sets preference matrices in an array. Has to be the same size as size of matrix provided in
     * setPrioritesMatrix method.
     * @param array $matrices
     */
    public function setPreferenceMatrices(Array $matrices) {
        $this->preferences = $matrices;
    }
    /**
     * Sets algorithm used to fill matrices. It has total control of what is filled
     * and how much.
     * @param type $generator
     */
    public function setMatrixGenerator(\AHP\Algorithms\MatrixGenerator $generator) {
        $this->generator = $generator;
    }
    /**
     * Sets algorithm used to fill the empty gaps in matrix.
     */
    public function setConsistencyAlgorithm(\AHP\Algorithms\ConsistencyAlgorithm $consistency) {
        $this->consistency = $consistency;
    }
    
    /**
     * Sets result extractor algorithm. It will take result from Solver algorithm and
     * generate result that is human-readable.
     */
    public function setResultExtractorAlgorithm(\AHP\Algorithms\ResultExtractor $extractor) {
        $this->extractor = $extractor;
    }
    
    /**
     * Sets algorithm used to normalize values during execution.
     */
    public function setNormalizer($algorithm) {
        if(!($algorithm instanceof \AHP\Algorithms\Normalizer) && !$this->checkMatricesOnSet) {
            throw new \UnexpectedValueException("Expected Elements Normalizer Algorithm.");
        }
        $this->normalizer = $algorithm;
    }
    
    /**
     * Sets algorithm used to solve the problem.
     */
    public function setSolverAlgorithm(\AHP\Algorithms\Solver $solver) {
        $this->solver = $solver;
    }
    /**
     * This method will take all of the defined values above and try to solve the problem. 
     * Then, the result extractor algorithm will come to extract result out of the 
     * list of generated results. The it will try to present the result in best way
     * possible.
     */
    public function solve() {
        if((!isset($this->criterias) || !isset($this->preferences)) && !isset($this->generator))
            throw new \RuntimeException("When using solver you must supply both criterias and preferences OR matrix generator.");
        if(!isset($this->solver))
            throw new \RuntimeException("You must supply a solver to be able to execute.");
        if(isset($this->consistency))
            $this->solver->setConsistencyAlgorithm($this->consistency);
        if(isset($this->preferences))
            $this->solver->setPreferenceMatrices ($this->preferences);
        if(isset($this->criterias))
            $this->solver->setPrioritesMatrix ($this->criterias);
        if(isset($this->generator))
            $this->solver->setMatrixExtractor ($this->generator);
        if(isset($this->normalizer))
            $this->solver->setNormalizer ($this->normalizer);
        if(!$this->solver->canSolve())
            throw new \RuntimeException("This solver can't solve this problem with supplied values.");
        $result = $this->solver->solve();
        if(isset($this->extractor)) {
            $this->extractor->setResult ($result);
            if($this->extractor->canExtract())
                return $this->extractor->extract();
        }
        return $result;
    }

}