<?php

namespace AHP\Algorithms;
/**
 * This class enables enables result extraction: process of generation a result
 * based on data generated by solver. Most of the times solver will generate thousands of matrices
 * or a single one. This result extractor must determine whether it can work
 * with this data type and must be able to extract data for further use.
 * Effect of this process is a Result record (that can be shown).
 */
abstract class ResultExtractor extends Algorithm {
    protected $result;
    protected $names;
    public function setResult($result) {
        $this->result = $result;
    }
    /**
     * Answers the question: can this extractor extract values from the dataset procided.
     */
    public abstract function canExtract();
    /**
     * Extracts data and generates Result.
     */
    public abstract function extract();
    
    public function setParamNames($params) {
        $this->names = $params;
    }
    
    
    protected function prepareObjectParamNames($n) {
        $result = array();
        for($i=0; $i<$n; $i++) {
            if($this->names!=null && $this->names["objects"]!=null && $this->names["objects"][$i]!=null)
                $result[$i] = $this->names["objects"][$i];
            else {
                $result[$i] = t("advanced_object")." ".($i+1);
            }
        }
        return $result;
    }
}
