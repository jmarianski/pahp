<?php


namespace AHP\Algorithms\Solvers;
/**
 * This solvers primary use is for manipulating data with uncertainities: whenever
 * we'd like to calculate something we're uncertain of, but it lies on normalized
 * axis of values from 0 to 1, we can randomize it N times, check how many times
 * one result was better than the other and use it as eigen vector for other calculations.
 * We assume that bigger value is better.
 */
class MonteCarlo extends \AHP\Algorithms\Solver {
    
    protected $n = 1;
    public function setPrioritesMatrix($matrix) {
        if(!$this->checkMatricesOnSet && !($matrix instanceof \Math\AbstractMatrix)) {
            throw new \UnexpectedValueException("Priorities matrix has to be single square matrix or function matrix.");
        }
        if(!$this->checkMatricesOnSet && isset($this->preferences) && count($this->preferences)!=$matrix->num_rows())
            throw new \UnexpectedValueException("Wrong params: number of matrices in preferences array is unequal to number or rows/cols in supplied priorities matrix.");
        $this->priorities = $matrix;
    }
    /**
     * Sets preference matrices in an array. Has to be the same size as size of matrix provided in
     * setPrioritesMatrix method.
     * @param array $matrices
     */
    public function setPreferenceMatrices($matrices) {
        if(!$this->checkMatricesOnSet && !is_array($matrices))
            throw new \UnexpectedValueException("Expected array of matrices");
        if(!$this->checkMatricesOnSet && isset($this->priorities) && $this->priorities->num_rows()!=count($matrices))
            throw new \UnexpectedValueException("Wrong params: number of matrices in supplied preferences array is unequal to number or rows/cols in priorities matrix.");
        if($this->checkMatricesOnSet)
            foreach($matrices as $m)
                if(!($m instanceof \Math\AbstractMatrix))
                    throw new \UnexpectedValueException("Array must be exclusively populated with matrices (square or function).");
        $this->preferences = $matrices;
    }
    /**
     * Sets number of tries per FunctionMatrix one has to do to determine final result of eigen vector for this comparison matrix.
     * @param type $n
     */
    public function setNumberOfTries($n) {
        $this->n = $n;
    }
    
    public function canSolve() {
        if(!($this->priorities instanceof \Math\AbstractMatrix) || !is_array($this->preferences))
            return false;
        if($this->priorities->num_rows()!=count($this->preferences))
            return false;
        foreach($this->preferences as $m)
            if(!($m instanceof \Math\AbstractMatrix))
                return false;
        return true;
    }

    public function getTypeOfResult() {
        return "array";
    }

    public function solve() {
        $result = array();
        for($i=0; $i<$this->n; $i++) {
            $all = array();
            $consistencies = array();
            $vectors = array();
            list($all[], $consistencies[]) = $this->priorities->calculateEigenVector();
            foreach($this->preferences as $matrix) {
                list($m, $consistencies[]) = $matrix->calculateEigenVector();
                $all[] = $vectors[] = $m;
            }
            foreach($consistencies as $index=>$c) {
                if($c>0.1 && !$this->fix($all[$i])) {
                    throw new \RuntimeException("Can't fix inconsistencies. Inconsistency found at $index, value: $c");
                }
            }
            
            $matrix = \Math\Matrix::concatenateLines($vectors);
            $result["preferences"][] = $matrix;
            $result["priorities"][] = $all[0];
            $result["output"][] = \Math\Matrix::multiply($matrix, $all[0]);
        }
        return $result;
    }
    

    public function getParams() {
        return array("n");
    }

}
