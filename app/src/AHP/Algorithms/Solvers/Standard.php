<?php

namespace AHP\Algorithms\Solvers;
/**
 * Standard solver takes matrices and generates single result. Nothing fancy.
 */
class Standard extends \AHP\Algorithms\Solver {
    public function canSolve() {
        if(!($this->priorities instanceof \Math\AbstractMatrix) || !is_array($this->preferences))
            return false;
        if($this->priorities->num_rows()!=count($this->preferences))
            return false;
        foreach($this->preferences as $m)
            if(!($m instanceof \Math\AbstractMatrix))
                return false;
        return true;
    }

    public function getTypeOfResult() {
        return "matrix";
    }

    public function solve() {
        list($all[], $consistencies[]) = $this->priorities->calculateEigenVector();
        foreach($this->preferences as $matrix) {
            list($m, $consistencies[]) = $matrix->calculateEigenVector();
            $all[] = $vectors[] = $m;
        }
        foreach($consistencies as $i=>$c) {
            if($c>0.1 && !$this->fix($all[$i])) {
                throw new \RuntimeException("Can't fix inconsistencies. Inconsistency found at $i, value: $c");
            }
        }
        $matrix = \Math\Matrix::concatenateLines($vectors);
        return \Math\Matrix::multiply($matrix, $all[0]);
    }

    public function getParams() {
        return array();
    }

//put your code here
}
