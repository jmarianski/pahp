<?php
namespace AHP\Algorithms;
/**
 * This class has one purpose: to allow us to define params, that are otherwise hidden.
 * This way we can set values for fields even if we don't have public access.
 * Also it provides informational function, with an indication, which fields should be
 * written via setParams. If you want to use this class for private params, override
 * it in your class.
 */
abstract class Algorithm {
    /**
     * Gets parameters for this algorithm.
     */
    public abstract function getParams();
    /**
     * Sets all the params. Obviously, if they are not specified, an error will
     * be launched. The worst fact: it will be fatal error, uncatchable.
     * @param Array $array Array pairs of key => value, where key specifies
     * param name.
     */
    public function setParams($array) {
        foreach($array as $key=>$value) {
            if($value != "" && $value!=null)
                $this->$key = $value;
        }
    }
}
