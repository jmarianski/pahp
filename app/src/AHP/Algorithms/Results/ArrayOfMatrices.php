<?php

namespace AHP\Algorithms\Results;
/**
 * Data held by this object is an array of matrices.
 */
class ArrayOfMatrices extends \AHP\Algorithms\Result {
    public $label;
    public $labels;
    public function canShow() {
        return is_array($this->data);
    }

    public function show() {
        $i = 0;
        if($this->label!=null)
            echo $label."<BR>";
        foreach($this->data as $matrix)
            if($matrix instanceof \Math\Matrix) {
                if($this->labels!=null && $this->labels[$i]!=null)
                    echo $this->labels[$i]."<BR>";
                else
                    echo t("array_of_matrices_array")." ".(++$i).":<BR>";
                $matrix->show ();
            }
    }

    
}
