<?php

namespace AHP\Algorithms\Results;
/**
 * Just a string to be printed between "pre" tags.
 */
class String extends \AHP\Algorithms\Result {
    public function canShow() {
        return true;
    }

    public function show() {
        echo "<pre>".$this->data."</pre>";
    }

}
