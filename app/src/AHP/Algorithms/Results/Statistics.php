<?php

namespace AHP\Algorithms\Results;
/**
 * Data held by this object is an array of objects and statistical measurements:
 * P_BEST, P_AT_LEAST, P_THRESHOLD, EXPECTED VALUE
 */
class Statistics extends \AHP\Algorithms\Result {
    
    private $names;
    
    public function canShow() {
        return isset($this->data["n"]) && is_array($this->data["p_best"]) && is_array($this->data["p_at_least"]) 
        && is_array($this->data["p_threshold"]) && is_array($this->data["ev"])
        && is_array($this->data["eu"]); // no need to check if they are filled, they have to be set.
    }

    public function show() {
        echo str_replace("$", $this->data["n"], t("statistics_header"));
        $n = count($this->data["p_best"]);
        $headers = array("p_best", "p_at_least", "p_threshold", "ev");
        $header_names = array("P_BEST", "P_AT_LEAST", "P_THRESHOLD", "EXPECTED VALUE");
        echo "<table border=1>"
        . "<tr>";
        echo "<td>".strtoupper(t("advanced_object"))."</td>";
        foreach($header_names as $h) {
            echo "<td>$h</td>";
        }
        echo "</tr>";
        for($i=0; $i<$n; $i++) {
            $j = $i+1;
            echo "<tr>";
            if($this->names==null || $this->names[$i]==null)
                echo "<td>".t("advanced_object")." $j</td>";
            else
                echo "<td>".$this->names[$i]."</td>";
            foreach($headers as $h)
                echo "<td>".$this->data[$h][$i]."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    
    public function setNames($names) {
        $this->names = $names;
    }

}
