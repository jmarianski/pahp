<?php


namespace AHP\Algorithms;
/**
 * This algorithm fixes inconsistencies.
 */
abstract class ConsistencyAlgorithm extends Algorithm {
    private $matrix;

    /**
     * Sets matrix to be solved.
     */
    public function setMatrix(\Math\SquareMatrix $matrix) {
        $this->matrix = $matrix;
    }
    /**
     * Checks if supplied matrix can be solved.
     */
    public abstract function canFix();
    /**
     * Returns filled matrix or throws an exception
     */
    public abstract function fix();
}
