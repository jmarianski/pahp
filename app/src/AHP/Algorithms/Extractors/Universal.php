<?php


namespace AHP\Algorithms\Extractors;
/**
 * Universal extractor. If he gets matrices in an array (like from MonteCarlo), 
 * it will print them in neat tables. If it gets single matrix, it will just show it.
 * In other cases it will use print_r function to generate a string that can be easily printed.
 */
class Universal extends \AHP\Algorithms\ResultExtractor {

    public function canExtract() {
        return true;
    }

    public function extract() {
        if($this->result instanceof \Math\Matrix) {
            $r = new \AHP\Algorithms\Results\ArrayOfMatrices();
            $r->setResult(array($this->result));
            return $r;
        }
        if(is_array($this->result) && count($this->result)>0) {
            if($this->result[0] instanceof \Math\Matrix) {
                $r = new \AHP\Algorithms\Results\ArrayOfMatrices();
                $r->setResult($this->result);
                return $r;
            } else if($this->result["output"][0] instanceof \Math\Matrix) {
                $r = new \AHP\Algorithms\Results\ArrayOfMatrices();
                $r->setResult($this->result["output"]);
                return $r;
            }
        }
        $r = new \AHP\Algorithms\Results\String();
        $r->setResult(print_r($this->result, true));
        return $r;
    }

    public function getParams() {
        return array();
    }

}
