<?php

namespace AHP\Algorithms\Extractors;
/**
 * This extractor takes results from MonteCarlo method and generates Statistics result.
 * Details in specific functions.
 */
class Statistics extends \AHP\Algorithms\ResultExtractor {
    protected $at_least_val = 2;
    protected $threshold = 0.25;
    public function canExtract() {
        return (is_array($this->result["output"]) 
                && count($this->result["output"])>0
                && $this->result["output"][0] instanceof \Math\Matrix  //assuming that every result is a matrix
                && $this->result["output"][0]->num_columns()==1); // assuming that every other matrix is an eigen vector
    }
    
    public function setThreshold($t) {
        $this->threshold = $t;
    }
    public function set_at_least($at_least) {
        $this->at_least_val = $at_least;
    }

    public function extract() {
        if(!$this->canExtract())
            throw new \RuntimeException("Can't extract: not an acceptable output format (must be array of arrays, where one array is named \"output\" and it consists of Matrices (eigen vectors).");
        $rows = $this->result["output"][0]->num_rows();
        $headers = array("p_best", "p_at_least", "p_threshold", "ev");
        $result = $this->fill_with_zeroes($rows, $headers);
        $result["n"] = $n = count($this->result["output"]);
        foreach($this->result["output"] as $matrix) {
            $result["p_best"][$this->p_best($matrix)]++;
            foreach($this->p_at_least($matrix) as $r) {
                $result["p_at_least"][$r]++;
            }
            foreach($this->p_over_threshold($matrix) as $r) {
                $result["p_threshold"][$r]++;
            }
            /* Expected value is an average of values. */
            for($i=0; $i<$rows; $i++)
                $result["ev"][$i] += $matrix->get($i,0);
        }
        $headers_to_avg = array("p_best", "p_at_least", "p_threshold", "ev");
        foreach($headers_to_avg as $h) {
            for($i=0; $i<$rows; $i++) {
                $result[$h][$i] /= $n;
            }
        }
        $res_obj = new \AHP\Algorithms\Results\Statistics();
        $res_obj->setResult($result);
        $res_obj->setNames($this->prepareObjectParamNames($rows));
        return $res_obj;
    }
    
    protected function fill_with_zeroes($rows, $headers) {
        $array = array();
        foreach($headers as $h) {
            $array[$h] = array_fill(0, $rows, 0);
        }
        return $array;
    }
    /**
     * Gets best algorithm in traditional AHP approach and counts down how many
     * times this algorithm won.
     * @param \Math\Matrix $matrix
     * @return int
     */
    private function p_best(\Math\Matrix $matrix) {
        $size = $matrix->num_rows();
        $max = -INF;
        $index = -1;
        for($j=0; $j<$size; $j++) {
            $val = $matrix->get($j,0);
            if($val>$max) {
                $max = $val;
                $index = $j;
            }
        }
        return $index;
    }
    /**
     * Helper function that extracts array from eigen vector. Useful for sort functions
     * that PHP supplies.
     * @param \Math\Matrix $m
     * @return type
     */
    private function convert_eigenvector_to_array(\Math\Matrix $m) {
        $array = $m->getArray();
        $result = array();
        for($i=0; $i<count($array); $i++)
            $result[] = $array[$i][0];
        return $result;
    }
    /**
     * Extracts N best AHP results.
     * @param \Math\Matrix $matrix
     * @return type
     */
    private function p_at_least(\Math\Matrix $matrix) {
        $size = $matrix->num_rows();
        $array = $this->convert_eigenvector_to_array($matrix);
        asort($array); // from worst to best
        $i = 0;
        $result = array();
        foreach($array as $key=>$_) {
            if($i++ >= $size - $this->at_least_val)
                $result[] = $key;
        }
        return $result;
    }
    /**
     * Extracts results better than threshold.
     * @param \Math\Matrix $matrix
     * @return int
     */
    private function p_over_threshold(\Math\Matrix $matrix) {
        $size = $matrix->num_rows();
        $result = array();
        for($j=0; $j<$size; $j++) {
            $val = $matrix->get($j,0);
            if($val>=$this->threshold) {
                $result[] = $j;
            }
        }
        return $result;
    }

    public function getParams() {
        return array("at_least_val", "threshold");
    }

}
