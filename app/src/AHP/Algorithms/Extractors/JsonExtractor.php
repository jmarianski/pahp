<?php


namespace AHP\Algorithms\Extractors;
/**
 * Universal extractor. If he gets matrices in an array (like from MonteCarlo), 
 * it will print them in neat tables. If it gets single matrix, it will just show it.
 * In other cases it will use print_r function to generate a string that can be easily printed.
 */
class JsonExtractor extends \AHP\Algorithms\ResultExtractor {

    public function canExtract() {
        return true;
    }

    public function extract() {
        $r = new \AHP\Algorithms\Results\Json();
        $r->setResult(json_encode($this->result));
        return $r;
    }

    public function getParams() {
        return array();
    }

}
