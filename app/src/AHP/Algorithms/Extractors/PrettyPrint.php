<?php


namespace AHP\Algorithms\Extractors;
/**
 * Universal extractor. If he gets matrices in an array (like from MonteCarlo), 
 * it will print them in neat tables. If it gets single matrix, it will just show it.
 * In other cases it will use print_r function to generate a string that can be easily printed.
 */
class PrettyPrint extends \AHP\Algorithms\ResultExtractor {

    public function canExtract() {
        return true;
    }

    public function extract() {
        if($this->result instanceof \Math\Matrix) {
            return $this->extractMatrix($this->result);
        }
        if(is_array($this->result) && count($this->result)>0) {
            if($this->result[0] instanceof \Math\Matrix) {
                return $this->extractArrayOfMatrices($this->result);
            } else if($this->result["output"][0] instanceof \Math\Matrix) {
                return $this->extractStatistics($this->result);
            }
        }
        $r = new \AHP\Algorithms\Results\String();
        $r->setResult(print_r($this->result, true));
        return $r;
    }
    
    
    private function extractMatrix(\Math\Matrix $matrix) {
        $params = $this->prepareObjectParamNames($matrix->num_rows());
        $matrix->setParamNames($params);
        $r = new \AHP\Algorithms\Results\ArrayOfMatrices();
        $r->setResult(array($matrix));
        return $r;
    }
    
    private function extractArrayOfMatrices($array) {
        $r = new \AHP\Algorithms\Results\ArrayOfMatrices();
        $r->setResult($array);
        return $r;
    }
    
    private function extractStatistics($array) {
        $s = new Statistics();
        $s->setResult($array);
        $s->setParamNames($this->names);
        return $s->extract();
    }

    public function getParams() {
        return array();
    }

}
