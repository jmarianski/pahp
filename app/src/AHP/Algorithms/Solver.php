<?php

namespace AHP\Algorithms;
/**
 * Main algorithm: solves the AHP problem. It can do it in many ways, for example Standard
 * algorithm will take matrices and generate single result, MonteCarlo will take supplied
 * propability function and generate array of eigen vectors, and other methods could use
 * matrix extractor to generate result, mix the methods, etc.
 */
abstract class Solver extends Algorithm {
    
    protected $priorities;
    protected $preferences;
    protected $consistencyAlgo;
    protected $generator;
    protected $normalizer;
    protected $checkMatricesOnSet = false;
    
    /**
     * This function determines what type of a result this Solver returns. It is useful
     * for Result extractor to know how to react to the result. Can be:<BR>
     * array - array of matrixes<BR>
     * matrix - single matrix with results<BR>
     * none - doesn't return any result.
     */
    public abstract function getTypeOfResult();    
    /**
     * Sets priority matrix (criterias). Has to be the same size as number of matrices provided in
     * setPreferanceMatrices method.
     * @param $matrix
     */
    public function setPrioritesMatrix($matrix) {
        if(!$this->checkMatricesOnSet && !($matrix instanceof \Math\SquareMatrix)) {
            throw new \UnexpectedValueException("Priorities matrix has to be single square matrix.");
        }
        if(!$this->checkMatricesOnSet && isset($this->preferences) && count($this->preferences)!=$matrix->num_rows())
            throw new \UnexpectedValueException("Wrong params: number of matrices in preferences array is unequal to number or rows/cols in supplied priorities matrix.");
        $this->priorities = $matrix;
    }
    /**
     * Sets preference matrices in an array. Has to be the same size as size of matrix provided in
     * setPrioritesMatrix method.
     * @param array $matrices
     */
    public function setPreferenceMatrices($matrices) {
        if(!$this->checkMatricesOnSet && !is_array($matrices))
            throw new \UnexpectedValueException("Expected array of matrices");
        if(!$this->checkMatricesOnSet && isset($this->priorities) && $this->priorities->num_rows()!=count($matrices))
            throw new \UnexpectedValueException("Wrong params: number of matrices in supplied preferences array is unequal to number or rows/cols in priorities matrix.");
        if($this->checkMatricesOnSet)
            foreach($matrices as $m)
                if(!($m instanceof \Math\SquareMatrix))
                    throw new \UnexpectedValueException("Array must be exclusively populated with matrices.");
        $this->preferences = $matrices;
    }
    /**
     * Sets algorithm used to fill the empty gaps in matrix.
     */
    public function setConsistencyAlgorithm($algorithm) {
        if(!($algorithm instanceof ConsistencyAlgorithm) && !$this->checkMatricesOnSet) {
            throw new \UnexpectedValueException("Expected Consistency Algorithm.");
        }
        $this->consistencyAlgo = $algorithm;
    }
    /**
     * Sets algorithm used to fill the empty gaps in matrix.
     */
    public function setMatrixExtractor($algorithm) {
        if(!($algorithm instanceof ConsistencyAlgorithm) && !$this->checkMatricesOnSet) {
            throw new \UnexpectedValueException("Expected Matrix Extractor Algorithm.");
        }
        $this->generator = $algorithm;
    }
    /**
     * Sets algorithm used to normalize values during execution.
     */
    public function setNormalizer($algorithm) {
        if(!($algorithm instanceof Normalizer) && !$this->checkMatricesOnSet) {
            throw new \UnexpectedValueException("Expected Elements Normalizer Algorithm.");
        }
        $this->normalizer = $algorithm;
    }
    /**
     * Checks whether all of the conditions have been met to solve this problem.
     */
    public abstract function canSolve();
    /**
     * This method will take all of the defined values above and try to solve the problem. 
     * Then, the result extractor algorithm will come to extract result out of the 
     * list of generated results. The it will try to present the result in best way
     * possible.
     */
    public abstract function solve();
    
    /**
     * Protected function fix - if the problem can be fixed with inconsistency,
     * it will try to do it or return false. 
     * @param \Math\AbstractMatrix $matrix
     * @return boolean
     */
    protected function fix(&$matrix) {
        if(isset($this->consistencyAlgo)) {
            $this->consistencyAlgo->setMatrix($matrix);
            if($this->consistencyAlgo->canFix())
                $this->consistencyAlgo->fix();
            else
                return false;
        }
        else
           return false;
        return true;
    }
}
