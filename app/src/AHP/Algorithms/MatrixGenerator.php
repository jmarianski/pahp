<?php

namespace AHP\Algorithms;
/**
 * This algorithm generates matrices for AHP algorithm.
 */
abstract class MatrixGenerator extends Algorithm {
    /**
     * If you have some prefilled matrices, supply them
     */
    public abstract function setGeneratorParams(\Math\Matrix $criterias, Array $preferences);
    /**
     * Generates matrices for single use of AHP algorythm.
     */
    public abstract function generate();
}
