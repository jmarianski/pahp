<?php


namespace AHP\Algorithms;
/**
 * Abstract class holding and presenting result. 
 */
abstract class Result {
    protected $data;
    /**
     * Sets raw data.
     * @param mixed $data
     */
    public function setResult($data) {
        $this->data = $data;
    }
    /**
     * Defines if the result can be shown. If not, then something must have gone wrong.
     */
    public abstract function canShow();
    /**
     * Shows value stored in $data variable.
     */
    public abstract function show();
    /**
     * Gets raw data supplied with setResult.
     * @return mixed
     */
    public function getRawData() {
        return $this->data;
    }
}
