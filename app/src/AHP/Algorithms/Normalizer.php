<?php


namespace AHP\Algorithms;
/**
 * This algorithm normalizes values in vectors. 
 */
abstract class Normalizer extends Algorithm {
    private $orders;
    
    public function __construct($orders) {
        if($orders!=null && is_array($orders))
            $this->orders = $orders;
    }
    /**
     * Normalizes points in array
     */
    public abstract function noramalizePoints(Array &$points);
    /**
     * Normalizes eigen vector
     */
    public abstract function noramalizeEigenVector(\Math\AbstractMatrix $m);
    /**
     * Accepts orders for what is needed to be normalized.
     * @param array $orders
     */
    public function whatNeedsToBeNormalized(Array $orders) {
        $this->orders = $orders;
    }
    /**
     * Checks orders if this is the moment to normalize.
     * @param type $string
     * @return boolean
     */
    protected function doNormalize($string) {
        foreach($this->orders as $s)
            if($s==$string)
                return true;
        return false;
    }
    
    
}
