<?php

namespace AHP\Algorithms;

class Linear  { // class Linear extends ConsistencyAlgorithm {
    
    public function canFix() {
        return true;
    }

    public function fix() {
        
    }
    
    private function y($n) {
        $y = \Math\Matrix::zeroes($n, $n-1);
        for($i=0; $i<$n-1; $i++) {
            for($j=0; $j<=$i; $j++) {
                $y->set($j, $i, 1);
            }
            $y->set($i+1, $i, -$i);
        }
        return $y;
    }
    
    private function matrix($A) {
        
    }
    
    private function log($matrix) {
        
    }

    public function getParams() {
        return array();
    }

//put your code here
}
