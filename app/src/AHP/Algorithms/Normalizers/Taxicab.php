<?php

namespace AHP\Algorithms\Normalizers;
/**
 * NORM function that takes sum of absolutes of values as basis for normalization.
 */
class Taxicab extends \AHP\Algorithms\Normalizer {
    
    public function noramalizeEigenVector(\Math\AbstractMatrix $m) {
        if(!$this->doNormalize("eigen"))
            return;
        $sum = 0;
        for($i=0; $i<$m->num_rows(); $i++)
            $sum += abs($m->get($i, 0));
        for($i=0; $i<$m->num_rows(); $i++)
            $m->set ($i, 0, $m->get($i, 0)/$sum);
    }

    /**
     * Dummy function, not used anywhere, because it seems faulty.
     */
    public function noramalizePoints(array &$points) {
        if(!$this->doNormalize("points"))
            return;
        $n = count($points);
        $sum = 0;
        foreach($points as $p)
            $sum += abs($p);
        $sum /= $n;
        foreach($points as &$p) {
        /*
         * 1. Make points relative to the average point
         * 2. Divide by average
         * 3. Moze the average point to 0.5
         */
            $p = ($p - $sum)/$sum + 0.5; 
        }
    }

    public function getParams() {
        return array();
    }

}
