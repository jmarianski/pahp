<?php
if($_GET["u"]=="")
	$_GET["u"] = "index";
define('BULLET_ROOT', dirname(__DIR__));
define('BULLET_APP_ROOT', BULLET_ROOT . '/app/');
define('BULLET_SRC_ROOT', BULLET_APP_ROOT . '/src/');

// Composer Autoloader
$loader = require BULLET_ROOT . '/vendor/autoload.php';
include("../pChart2.1.4/class/pData.class.php");
include("../pChart2.1.4/class/pDraw.class.php");
include("../pChart2.1.4/class/pImage.class.php");
$arr = glob("../app/src/*");
for($i=0; $i<count($arr); $i++){
    $a = $arr[$i];
    $path = explode("/", $a);
    $path2 = explode("\\", $path[count($a)-1]);
    $filename = $path2[count($path2)-1];
    if(is_file($a) && pathinfo($a, PATHINFO_EXTENSION)=="php")
        require_once($a);
    if(is_dir($a)) {
        $arr = array_merge ($arr, glob($a."/*"));
    }
}

// Bullet App
$app = new Bullet\App(require BULLET_APP_ROOT . 'config.php');
$request = new Bullet\Request();

// Common include
require BULLET_APP_ROOT . '/common.php';

// Require all paths/routes
$routesDir = BULLET_APP_ROOT . '/routes/';
$files = glob($routesDir."*.*");
foreach($files as $f) {
    require $f;
}
// CLI routes
if($request->isCli()) {
    require $routesDir . 'db.php';
}
$app["urls"] = function() {
    $urls = t("top_url");
    return $urls;
};

// Response
echo $app->run($request);

