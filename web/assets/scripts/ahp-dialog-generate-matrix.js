$("button#arbitral").click(arbitralClicked);

function arbitralClicked() {
    var n = getVisibleMatrix().find("tr").length - 1;
    var popup = window.open('popup_generate_matrix?n='+n, '_blank', 'top=300, left=300, width=600, height=500');
    popup.onbeforeunload = popupClosing;
}

function popupClosing() {
    var val = $(this.document.documentElement).find("input[name='value']").val();
    if(val.length>0) {
        var array = JSON.parse(val);
        var matrix = getVisibleMatrix();
        var id = matrix.prop("id");
        for(var i=0; i< array.length; i++) {
            for(var j=0; j< array.length; j++) {
                var val = array[i]/array[j];
                if(array[i]!=null && array[j]!=null) {
                    var name = "matrix["+id+"][data]["+i+"]["+j+"]";
                    $("input[name='"+name+"']").val(val);
                    $("input[name='"+name+"']").trigger("change");
                }
            }
        }
    }
}

function getVisibleMatrix() {
    var matrix = $("div.matrix").filter(visibility_filter);
    if(matrix.hasClass("regular"))
        return matrix;
}