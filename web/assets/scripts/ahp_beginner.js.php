<?php
/* 
 * This file isn't meant to be included on it's own with <script> tag. Instead,
 * use php's function include.
 * 
 */?>
<script>
$(document).ready(function() {
    var steps = <?php
    $steps = array();
foreach(t("beginner_steps") as $n=>$step) {
    $s = array("n"=>$n,
        "header" => str_replace("$", $n+1, t("beginner_step_N")),
        "text" => ""
        );
    if(is_array($step))
        foreach($step as $line)
            $s["text"] .= "<p>$line</p>\n";
    else
        $s["text"] .= "<p>$step</p>\n";
    $steps[] = $s;
}
echo json_encode($steps);
    ?>;
    var comparison_tooltips = <?=json_encode(t("beginner_pairwise_comparisons"))?>;
    step = 0;
    var changes = [];
    var matrix = 1;
    var before;
    $("select, input").on("change", function() {onchange($(this), $(this).val());});
    $("input[name='num_pref'], input[name='num_crit']").on("change", function() {
        if(typeof num_pref != 'undefined' && typeof num_crit != 'undefined') {
            if($(this).attr("name")=="num_pref") {
                $(this).attr("placeholder", num_pref);
                if($(this).val()!=num_pref)
                    $(this).css("background-color", "red");
                else
                    $(this).css("background-color", "");
            }
            if($(this).attr("name")=="num_crit") {
                $(this).attr("placeholder", num_crit);
                if($(this).val()!=num_crit)
                    $(this).css("background-color", "red");
                else
                    $(this).css("background-color", "");
            }
        }
    });
    function onchange(obj, after) {
        var step_now = step;
        var to_push = {
            type:"change",
            name:obj.attr("name"),
            value:after
        };
        if(!$.isArray(changes[step_now]))
            changes[step_now] = [];
        changes[step_now].push(to_push);
    };
    function next_step() {
        if(step==0) {
            if(check_changed_base_values()) // if handled
                return;
            if(!base_button_clicked())
                return;
            else {
                attach_onchange($("div.matrix"));
            }
        }
        if(step==2) {
            var solver = $("select[name='algo[0]']");
            var result = $("select[name='algo[4]']");
            if(solver.val()=="") {
                solver.val("AHP\\Algorithms\\Solvers\\Standard");
                solver.trigger("change");
            }
            if(result.val()=="") {
                result.val("AHP\\Algorithms\\Extractors\\PrettyPrint");
                result.trigger("change");
            }
        }
        if(step==5) {
            $("button#execute").click();
            return;
        }
        if(step==4) {
            if(matrix>=num_crit)
                step++;
            else
                matrix++;
        }
        else
            step++;
        get_step();
    };
    function check_changed_base_values() {
        if(typeof num_pref != 'undefined' && typeof num_crit != 'undefined') {
            if($("input[name='num_pref']").val()==num_pref && $("input[name='num_crit']").val()==num_crit) {
                step++;
                get_step();
                return true;
            } else {
                if(!confirm("<?=t("beginner_base_changes_warning")?>"))
                    return true;
                else {
                    $("input[name='num_pref'], input[name='num_crit']")
                            .css("background-color", "");
                }
            }
        }
        return false;
    }
    function revert_change(bool, s) {
        var step;
        if(s==null)
            step = window.step;
        else
            step = s;
        if(typeof changes[step]=='undefined' || changes[step].length==0)
            return;
        if(typeof changes[step][0]=='undefined')
            alert(JSON.stringify(changes));
        var change = changes[step].pop();
        if(typeof change=='undefined')
            return;
        var name = change.name.split("\\").join("\\\\");
        var obj = $("[name*='"+name+"']");
        var j = -1;
        for(var i=0; i<changes[step].length; i++) {
            if(changes[step][i].name==change.name)
                j = i;
        }
        before = "";
        if(j>-1) {
            before = changes[step][j].value;
        }
        obj.val(before);
        if(!bool || typeof bool == 'object') {
            obj.change();
            changes[step].pop();
        }
    };
    function prev_step() {
        if(step==0) {
            return;
        }
        if(step==4 && matrix>1) {
            matrix--;
        }
        else
            step--;
        get_step();
    };
    function update_pairwise_comparison() {
        var m = matrix;
        if(step==3)
            m = 0;
        //comparison_tooltips, pairwise_helper
        var mat = $("div.matrix#"+m);
        if(mat.hasClass("func") || mat.hasClass("fuzzy")) {
            $(".pairwise_helper").css("display", "none");
            return;
        }
        $(".pairwise_helper").css("display", "");
        var pair = get_first_pairwise_unfilled(mat, m);
        if(pair==null) {
            finished_comparisons(mat)
            return;
        } 
        $(".pairwise").css("display", "");
        $(".finished_comparison").css("display", "none");
        var i = pair[0];
        var j = pair[1];
        set_label(mat, i, j);
        blockPairwiseButtons(mat,i,j);
        var label1 = getLabelFromMatrix(mat, i);
        var label2 = getLabelFromMatrix(mat, j);
        $(".object_1").html(label1);
        $(".object_2").html(label2);
        var td = $("table.pairwise td.select");
        var numbers = [9,7,5,3,1,3,5,7,9];
        var order =   [1,1,1,1,1,2,2,2,2];
        $.each(td, function(key, value) {
            var tooltip = comparison_tooltips[numbers[key]];
            if(order[key]==1) {
                tooltip = tooltip.replace("$", "\""+label1+"\"");
                tooltip = tooltip.replace("$", "\""+label2+"\"");
            } else {
                tooltip = tooltip.replace("$", "\""+label2+"\"");
                tooltip = tooltip.replace("$", "\""+label1+"\"");
            }
            $(value).find("span").html(tooltip);
        });
    };
    function finished_comparisons(matrix) {
        $("table.pairwise").css("display", "none");
        $("div.pairwise").html("<?=t("beginner_finished_comparisons")?>");
        $(".finished_comparison").css("display", "");
        var array = convertTableToArray(matrix);
        if(!is_matrix_consistent(array, array.length))
            $("div.pairwise").append("<BR><B><?=t("beginner_wrong_choices")?></B>");
    }
    function getLabelFromMatrix(matrix, i) {
        return matrix.find("table tr:nth-child("+(i+2)+") td:nth-child(1)").html();
    }
    function blockPairwiseButtons(visible_matrix, x,y) {
        if(typeof visible_matrix != 'undefined') {
            var matrix_size = visible_matrix.find("tr").length-1;
            var matrix = getBest(visible_matrix,x,y);
            var blocked_all = true;
            $("input[name='pairwise']").each(function(index,obj) {
                var val = $(obj).val();
                var left = val%2==1;
                if(!left)
                    val++;
                var is_valid = is_valid_value_for_matrix(matrix, val, !left, matrix_size);
                var dont_block_buttons = !$("input[name='dont_block_buttons']").prop("checked");
                    $(obj).prop("disabled", dont_block_buttons && !is_valid);
                if(is_valid)
                    blocked_all = false;
            });
            if(blocked_all)
                $("div.pairwise").append("<BR><B><?=t("beginner_wrong_choices")?></B>");
                
        }
    }
    function set_label(m, i, j) {
        if(m.prop("id")=="0")
            var label_template = <?=json_encode(t("beginner_criterium_A_over_B"))?>;
        else {
            var criterium = getLabelFromMatrix($("div.matrix#0"), matrix-1);
            var label_template = <?=json_encode(t("beginner_in_criterium_A_over_B"))?>;
            label_template = label_template.replace("\$", criterium);
        }
        var label1 = getLabelFromMatrix(m, i);
        var label2 = getLabelFromMatrix(m, j);
        var label = label_template.replace("\$", label1).replace("\$", label2);
        $("div.pairwise").html(label);
        
    }
    function animate_labels() {
        $(".object_1").fadeOut(10).fadeIn(500);
        $(".object_2").fadeOut(10).fadeIn(500);
        if($("div.pairwise").css("display")!="none")
            $("div.pairwise").fadeOut(10).fadeIn(500);
    };
    function getMatrixElement(num,i,j) {
        var name = "matrix["+num+"][data]["+i+"]["+j+"]";
        return $("input[name='"+name+"']");
    };
    function get_first_pairwise_unfilled(mat, mat_num) {
        var max = mat.find("table tr:nth-child(1) td").length - 1;
        var arr = null;
        for(var i=0; i<max; i++) {
            for(var j=0; j<=i; j++) {
                var elem = getMatrixElement(mat_num,i,j);
                var elem2 = getMatrixElement(mat_num,j,i);
                if(j==i && elem.val()!=1)
                    elem.val("1");
                else if(arr==null 
                        && ((typeof elem.val() == 'undefined' || elem.val()<=0)
                        || (typeof elem2.val() == 'undefined' || elem2.val()<=0)))
                    arr = [i,j];
            }
        }
        return arr;
    }
    $("input[type='radio'][name='pairwise']").off();
    $("input[type='radio'][name='pairwise']").change(function(){
        var m = matrix;
        if(step==3)
            m = 0;
        var mat = $("div.matrix#"+m);
        $(".pairwise_helper").css("display", "");
        var pair = get_first_pairwise_unfilled(mat, m);
        var val = parseInt($(this).val());
        if(!(val>0) || pair==null)
            return;
        var name1 = "matrix["+m+"][data]["+pair[0]+"]["+pair[1]+"]";
        var name2 = "matrix["+m+"][data]["+pair[1]+"]["+pair[0]+"]";
        var elem1 = $("input[name='"+name1+"']");
        var elem2 = $("input[name='"+name2+"']");
        if(val%2==0) {
            val +=1;
            elem1.val(1.0/val);
            elem2.val(val);
        } else {
            elem1.val(val);
            elem2.val(1.0/val);
        }
        $(this).prop("checked", false);
        update_pairwise_comparison();
        elem2.trigger("change");
        elem1.trigger("change");
        animate_labels();
        
    });
    $("input[name='dont_block_buttons']").change(function() {
        update_pairwise_comparison();
    });
    function get_step() {
        if(step==1) {
            $("#show_hide_names").remove();
            $("div.names.hide").removeClass("hide");
        }
        if(step>=1) {
            $("form#serialize button").prop("disabled", false);
            $("div.matrix").each(function() {
                var is_func = $(this).hasClass("func");
                if($("select[name='algo[0]']").val()=="AHP\\Algorithms\\Solvers\\Standard" && is_func) {
                    $("select[name='algo[0]']").val("AHP\\Algorithms\\Solvers\\MonteCarlo");
                    $("input[name='params[AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo][n]']").val("1000");
                    $("select[name='algo[0]']").trigger("change");
                    $("input[name='params[AHP\\\\Algorithms\\\\Solvers\\\\MonteCarlo][n]']").trigger("change");
                }
            });
        }
        $("[class*='step[']").css("display", "none");
        if(step==4) {
            $("[class*='step["+step+"]'][class*='matrix["+matrix+"]']").css("display", "");
        }
        else
            $("[class*='step["+step+"]']").css("display", "");
        if(step==5) {
            $("[class*='step[']").css("display", "");
            $("[class*='step[0]']").css("display", "none");
            $(".pairwise_helper").css("display", "none");
           $("select, input:not([type='hidden'])").prop("disabled", true);
        } else {
            $("select, input[type!='radio']").prop("disabled", false);
        }
        $("h2.header").html(steps[step].header);
        $("div.text_content").html(steps[step].text);
        if(step==3 || step==4) {
            update_pairwise_comparison();
        }
    };
    after_load = function() {
            $("#show_hide_names").remove();
            $("div.names.hide").removeClass("hide");
        var matrices = false;
        var id = 9999;
        $.each($("div.matrix td input"), function(object) {
            if(!matrices && typeof $(this).val() != 'undefined' && $(this).val() != '') {
                matrices = true;
            }
            var this_id = $(this).parents("div.matrix").attr("id");
            if(matrices && $(this).val()<=0 && this_id<id)
                id = this_id;
        });  
        attach_onchange($("div.matrix"));
        if(typeof num_pref != 'undefined')
            step = 1;
        if(JSON.stringify(formToObject($("form#algorithms")[0]))!="false")
            step = 2;
        if(matrices) {
            if(id == 0)
                step = 3;
            else if(id==9999) {
                step = 5;
            } else {
                step = 4;
                matrix = id;
            }
        }
        get_step();
    };
    function attach_onchange(form) {
        var inputs = form.find("input, select");
        inputs.each(function(i,input) {
            $(input).off("change");
            $(input).on("change", function() {
                update_pairwise_comparison();
                var obj = $(this);
                onchange(obj, obj.val());
            });
        });
        $("button.transform").click(function() {
            attach_onchange($("div.matrix"));
            update_pairwise_comparison();
        });
    }
    $("#prev_step").click(prev_step);
    $("#prev_change").click(revert_change);
    $("#next_step").click(next_step);
    get_step();
});
</script>