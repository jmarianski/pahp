<?php
/* 
 * This file isn't meant to be included on it's own with <script> tag. Instead,
 * use php's function include.
 * 
 */
 ?>
<script>
    var knows = false;
    var after_load = null;
    function base_button_clicked() {
        var temp_num_crit = $("input[name='num_crit']").val();
        var temp_num_pref = $("input[name='num_pref']").val();
        if(temp_num_crit<1 || temp_num_pref<1) {
            alert("<?=t("advanced_fill_required")?>");
            return false;
        }else {
            num_crit = $("input[name='num_crit']").val();
            num_pref = $("input[name='num_pref']").val();
            addMatrices();
            return true;
        }
    }
    $("#base button").on("click", base_button_clicked);
    $("#serialize").on("submit", function() {
        $(this).find("input").val(serializeForm());
        return true;
    });
    $("#file").on("submit", function() {
        var files = $(this).find(".file").prop('files');
        var file_data = files[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);              
        $.ajax({
            type:"POST", 
            url:"deserialize.php",
            contentType: false,
            processData: false, 
            data:form_data, 
            success: function(data) {
                deserializeFromAjax(data);
            }
        });
        return false;
    });
    $("#execute").on("click", function() {
        $.ajax({
            type:"POST", 
            url:"/pahp/ahp",
            data:JSON.parse(serializeForm()), 
            success: function(data) {
                $("#results").append(data);
                window.scrollTo(0,document.body.scrollHeight);
                if(document.hidden)
                    alert("<?=t("advanced_execution_completed")?>");
            }
        }).fail(function(data) {
                alert("error: "+data.status+"\n"+data.statusText+"\n"+data.responseText);
            });
    });
    $(document).ready(function() {
        $("select.algorithm").on("change", function() {algo_changed(this);});
    });
    function algo_changed(obj) {
        var all_algo = [];
        var i = 0;
        $(obj).find("option").each(function() {
            all_algo[i++] = $(this).val().split("\\").join(" ");
        });
        var name = $(obj).find("option:selected").val().split("\\").join(" ");
        for(var i=0; i<all_algo.length; i++) {
            if(all_algo[i] != "") {
                var string = "div."+(all_algo[i].split(" ").join("."));
                $(string).css("display", "none");
            }
        }
        if(name != "") {
            $("div."+(name.split(" ").join("."))).css("display", "block");
        }
    };
    function deserializeFromAjax(string) {
        var obj = JSON.parse(string);
        $("input[name='num_crit']").val(obj["base"]["num_crit"]);
        $("input[name='num_pref']").val(obj["base"]["num_pref"]);
        addMatrices(obj["matrices"]);
        if(obj["base"]["names"]!=null) {
            $.each(obj["base"]["names"], function (type, array) {
                if(array !=null) {
                    $.each(array, function(number, value) {
                        var name = "names["+type+"]["+number+"]";
                        $("input[name='"+name+"']").val(value);
                        $("input[name='"+name+"']").trigger("change");
                    });
                }
            });
        }
        var algo = obj["algorithms"]["algo"];
        for(var k in algo) {
            var a = $("select[name='algo["+k+"]']");
            a.val(algo[k]);
            a.trigger("change");
            a.each(function() {algo_changed(this);});
        }
        var params = obj["algorithms"]["params"];
        for(var k in params) {
            for(var p in params[k]) {
                // absurd, but works
                var ktemp = k.split("\\").join("\\\\");
                var ptemp = p.split("\\").join("\\\\"); // just to be sure
                $("input[name='params["+ktemp+"]["+ptemp+"]']").val(params[k][p]);
            }
        }
        if(after_load!=null)
            after_load();
    }
    function addMatrices(data) {
        $(".hide").removeClass("hide");
        $("#matrices").html("");
        num_crit = $("input[name='num_crit']").val();
        num_pref = $("input[name='num_pref']").val();
        generateFormForCriteriaAndObjectNames(num_crit,num_pref);
            var div = $("<div class=\"step[3] matrix[0]\">");
            div.append("<h2><?=t("advanced_criteria_matrix")?></h2>");
            div.append(addMatrix(0, getType(data, 0), num_crit, true, getData(data, 0)));
            $("#matrices").append(div);
        for(var i=1; i<=num_crit; i++) {
            var div = $("<div class=\"step[4] matrix["+i+"]\">");
            div.append("<h2 class=\"criterium_title_"+(i-1)+"\"><?=t("advanced_preference_matrix")?> "+(i)+"</h2>");
            div.append(addMatrix(i, getType(data, i), num_pref, false, getData(data, i)));
            $("#matrices").append(div);
        }
    };
    function getType(data, i) {
        if(data==null)
            return "regular";
        else {
            return data["matrix"][i]["type"];
        }
    }
    function getData(data, i) {
        if(data==null)
            return null;
        else {
            return data["matrix"][i]["data"];
        }
    }
    function addMatrix(id, type, size, crit, data) {
        var matrix = createEmptyMatrix(id, type, size, crit);
        setMatrixData(id, matrix, type, data);
        return matrix;
    };
    function createEmptyMatrix(id, type, size, crit) {
        var matrix;
        var button;
        if(type=="func") {
            matrix = recreateMatrixFromTemplate(id, crit, size);
        } else if(type=="fuzzy")  {
            matrix = generateFuzzyMatrix(id, crit, size);
        } else  {
            matrix = generateRegularMatrix(id, crit, size);
        }
        button = matrix.find("button.transform");
        addTransformFunctionality(button);
        return matrix;
    }
    function recreateMatrixFromTemplate(id, crit, size) {
            var matrix = $("<div id=\""+id+"\"  class=\"matrix "+(crit?"criteria":"preference")+" func\"></div>");
            for(var i=0; i<size; i++) {
                var form = $($(".form_func_hidden")[0].outerHTML);
                form.removeClass("form_func_hidden");
                form.find("select, input").each(function() {
                    var c = $(this).attr("class");
                    $(this).attr("name", "matrix["+id+"][data]["+i+"]["+c+"]"); // matrix id, element id, param id
                });
                attachHoverImage(form);
                addShowHide(form);
                matrix.append(form);
            }
            matrix.append("<input type='hidden' name=\"matrix["+id+"][type]\" value=\"func\">");
            var i = 0;
            matrix.children().each(function() {
                var c = (crit?"criterium_name_":"object_name_")+i;
                $(this).prepend("<div class=\""+c+"\">"+(crit?"<?=t("advanced_criterium")?>":"<?=t("advanced_object")?>")+" "+(++i)+"</div>");
            });
            matrix.append("<div><button type=\"button\" class=\"transform\"><?=t("advanced_transform")?></button></div>");
            return matrix;
    }
    function generateRegularMatrix(id, crit, n) {
        var html = "<div id=\""+id+"\" class='matrix "+(crit?"criteria":"preference")+" regular'><table border=1>";
        html += "<tr><td></td>";
        html +="<input type='hidden'  name=\"matrix["+id+"][type]\" value=\"regular\">";
        for(var i=1; i<=n; i++) {
            var c = (crit?"criterium_name_":"object_name_")+(i-1);
            if(crit)
                html +="<td class=\""+c+"\"><?=t("advanced_criterium")?> "+i+"</td>";
            else
                html +="<td class=\""+c+"\"><?=t("advanced_object")?> "+i+"</td>";
        }
        html+="</tr>";
        for(var i=1; i<=n; i++) {
            html += "<tr>";
            var c = (crit?"criterium_name_":"object_name_")+(i-1);
            if(crit)
                html +="<td class=\""+c+"\"><?=t("advanced_criterium")?> "+i+"</td>";
            else
                html +="<td class=\""+c+"\"><?=t("advanced_object")?> "+i+"</td>";
            for(var j=1; j<=n; j++)
                html += "<td><input name=\"matrix["+id+"][data]["+(i-1)+"]["+(j-1)+"]\"></td>";
        }
        html += "</table><button type=\"button\" class=\"transform\"><?=t("advanced_transform")?></button></div>";
        var matrix = $(html);
        for(var i=0; i<n; i++) {
            var name = "matrix["+id+"][data]["+i+"]["+i+"]";
            matrix.find("input[name='"+name+"']").val("1")
        }
        return matrix;
    };
    function generateFuzzyMatrix(id, crit, n) {
        var matrix = generateRegularMatrix(id, crit, n);
        matrix.find("table tr:nth-child(1) td:nth-child(1)").html("<B><?=t("fuzzy_fuzzy_short")?></B>");
        matrix.removeClass("regular");
        matrix.addClass("fuzzy");
        matrix.find("table").after("<?=t("fuzzy_doubleclick")?><BR>");
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            matrix.find("input").on('click', fuzzy_onclick);
        } else {
            matrix.find("input").on('dblclick', fuzzy_onclick);
        }
        matrix.find("input[name='matrix["+id+"][type]']").val("fuzzy");
        for(var i=0; i<n; i++) {
            var name = "matrix["+id+"][data]["+i+"]["+i+"]";
            matrix.find("input[name='"+name+"']").val("[[1,1,1]]")
        }
        return matrix;
    }
    function fuzzy_onclick() {
            var input = $(this);
            var name = input.attr("name");
            var split = name.split("[");
            var id = parseInt(split[1].substring(-1));
            var x = parseInt(split[3].substring(-1));
            var y = parseInt(split[4].substring(-1));
            if(x==y) {
                input.val("[[1,1,1]]");
                input.trigger("change");
                return;
            }
            var popup = window.open('popup_generate_fuzzy?val='+input.val(), '_blank', 'top=300, left=300, width=600, height=500');
            popup.onbeforeunload = function() {
            var val = $(this.document.documentElement).find("input[name='value']").val();
                if(val.length>0) {
                    var inverse = fuzzy_value_inverse(val);
                    input.val(val);
                    input.trigger("change");
                    var name = "matrix["+id+"][data]["+y+"]["+x+"]";
                    input.parents("div.matrix").find("input[name='"+name+"']").val(inverse);
                    input.parents("div.matrix").find("input[name='"+name+"']").trigger("change");
                }
            };
    }
    function fuzzy_value_inverse(fuzzy_value) {
        var value = JSON.parse(fuzzy_value);
        var result = [];
        for(var i=0; i<value.length; i++) {
            result.push([1/value[i][2], 1/value[i][1],1/value[i][0]]);
        }
        if(result.length>0)
            return JSON.stringify(result);
        else
            return;
    }
    function setMatrixData(id, matrix, type, data) {
        if(data==null)
            return;
        if(type=="func") {
            $.each(data, function(i, attributes) {
                $.each(attributes, function(attr, value) {
                    var name = "matrix["+id+"][data]["+i+"]["+attr+"]";
                    matrix.find("[name=\""+name+"\"]").val(value);
                });
            });
            matrix.find("input, select").trigger("change");
        } else if(type=="regular" || type=="fuzzy") {
            $.each(data, function(row, columns) {
                $.each(columns, function(column, value) {
                    var name = "matrix["+id+"][data]["+row+"]["+column+"]";
                    matrix.find("[name=\""+name+"\"]").val(value);
                });
            });
        }
    }
    function attachHoverImage(form) {
        var img = form.find("img");
        form.on("change", function() {
            var indexes = ["class", "points", "h", "min", "max", "step"];
            var values = [];
            for(var i=0; i<indexes.length; i++)
                values[i] = indexes[i]+"="+(form.find("."+indexes[i]).val());
            var string = values.join("&");
            string +="&method=pdf";
            var url = "chart.php?"+string;
            img.attr("src", "web/assets/images/loading.gif");
            var load = new Image();
            load.onload = function() {
                img.attr("src", url);
            };
            load.src = url;
            var saveimg = form.find("a.save_img");
            saveimg.attr("download", "chart_"+string.replace(/\.|=/g, "_")+".png");
            saveimg.attr("href", url);
        });
        img.hide();
        form.find("div").mousemove(function(e) {
            img.stop(1, 1).fadeIn();
            var width = window.innerWidth;
            var height = window.innerHeight;
            if(e.pageX<width/2 && e.clientY<height/2) {
                img.offset({
                    top: e.pageY + 30,
                    left: e.pageX
                });
            } else if(e.pageX>=width/2 && e.clientY<height/2) {
                img.offset({
                    top: e.pageY + 30,
                    left: e.pageX - (img.outerWidth())
                });
            } else if(e.pageX<width/2 && e.clientY>=height/2) {
                img.offset({
                    top: e.pageY - (img.outerHeight()) - 20,
                    left: e.pageX
                });
            } else if(e.pageX>=width/2 && e.clientY>=height/2) {
                img.offset({
                    top: e.pageY  - (img.outerHeight()) - 20,
                    left: e.pageX - (img.outerWidth())
                });
            }
        }).mouseleave(function() {
            img.fadeOut();
        });
        form.trigger("change");
    }
    function addShowHide(form) {
        form.find("div.short_form:not(button), div button.hide_button").click(function() {
            var tag = $(this).prop("tagName");
            if(tag=="DIV") {
                form.find(".long_form").css("display", "");
                $(this).css("display", "none");
            } else {
                form.find(".short_form").css("display", "");
                form.find(".long_form").css("display", "none");
            }
            return true;
        });
    }
    function addTransformFunctionality(button) {
        button.click(function() {
            if(confirm("<?=t("advanced_transform_confirm")?>")) { 
                var parent = $(button).parents("div.matrix");
                var crit = parent.hasClass("criteria");
                var id = parent.attr("id");
                var matrix;
                if(parent.hasClass("func")) {
                    var n = parent.children(".param").length;
                    matrix = createEmptyMatrix(id, "fuzzy", n, crit);
                    $(".pairwise_helper").css("display", "none");
                } else if(parent.hasClass("fuzzy")) {
                    var n = parent.find("table tr").length - 1;
                    matrix = createEmptyMatrix(id, "regular", n, crit);
                    $(".pairwise_helper").css("display", "");
                } else {
                    var n = parent.find("table tr").length - 1;
                    matrix = createEmptyMatrix(id, "func", n, crit);
                    $(".pairwise_helper").css("display", "none");
                }
                parent.replaceWith(matrix);
            }
        });
    }
    function serializeForm() {
        var were_disabled = false;
        $("input, select").each(function() {
            if($(this).prop("disabled"))
                were_disabled = true;
            $(this).prop("disabled", false);
        });
        $("input[name='num_crit']").val(num_crit);
        $("input[name='num_pref']").val(num_pref);
        var base = JSON.stringify(formToObject($("form#base")[0]));
        var matrices = JSON.stringify(formToObject($("form#matrices")[0]));
        var algo = JSON.stringify(formToObject($("form#algorithms")[0]));
        if(were_disabled) {
            $("select, input:not([type='hidden'])").prop("disabled", true); 
        }
        return "{\"base\": "+base+", \"matrices\": "+matrices+", \"algorithms\": "+algo+"}";
    }
    function generateFormForCriteriaAndObjectNames(c,o) {
        var html = "<div class=\"names\"><button id=\"show_hide_names\" type=\"button\"><?=t("advanced_show_names")?></button></div>\n";
        html += "<div class=\"names hidden hide step[1]\">";
        html += "<table border=1>\n";
        for(var i=1; i<=c; i++) {
            html += "<tr>\n";
            html +="<td><?=t("advanced_criterium")?> "+i+"</td>\n";
            html +="<td><input name='names[criteria]["+(i-1)+"]'></td>\n";
            html += "</tr>\n";
        }
        for(var i=1; i<=o; i++) {
            html += "<tr>\n";
            html +="<td><?=t("advanced_object")?> "+i+"</td>\n";
            html +="<td><input name='names[objects]["+(i-1)+"]'></td>\n";
            html += "</tr>\n";
        }
        html += "</table></div>\n";
        var obj = $(html);
        $("form#base .names").remove();
        $("form#base").append(obj);
        $("#show_hide_names").on("click", function() {
            $(".names.hidden").each(function() {
                if($(this).hasClass("hide")) {
                    $(this).removeClass("hide");
                    $("#show_hide_names").html("<?=t("advanced_hide_names")?>");
                }
                else {
                    $(this).addClass("hide");
                    $("#show_hide_names").html("<?=t("advanced_show_names")?>");
                }
            });
        });
        $(".names input").change(function() {
            var name = $(this).attr("name");
            var arr = name.split("[");
            var crit_num = arr[2].substring(0, arr[2].length-1);
            if(arr[1]=="criteria]") {
                updateCriteriaName(crit_num, $(this).val());
            } else {
                updateObjectName(crit_num, $(this).val());
            }
        });
    };
    function updateCriteriaName(crit_num, crit_name) {
        crit_num = parseInt(crit_num);
        var genname = "criterium_name_"+crit_num;
        var genname2 = "criterium_title_"+crit_num;
        var genval2 = "<?=t("advanced_criterium")?> \""+crit_name+"\": <?=t("advanced_preference_matrix")?>";
        var def_val = "<?=t("advanced_criterium")?> " + (crit_num+1);
        var def_val2 = "<?=t("advanced_preference_matrix")?> "+(crit_num+1);
        if(crit_name=="") {
            crit_name = def_val;
            genval2 = def_val2;
        }
        $("."+genname).html(crit_name);
        $("."+genname2).html(genval2);
    }
    function updateObjectName(crit_num, crit_name) {
        crit_num = parseInt(crit_num);
        var genname = "object_name_"+crit_num;
        var def_val = "<?=t("advanced_object")?> " + (crit_num+1);
        if(crit_name=="") {
            crit_name = def_val;
        }
        $("."+genname).html(crit_name);
    };
    window.onerror = function(error) {
        alert(<?=json_encode(t("ahp_error_javascript"))?>);
    };
</script>