function getVisibleMatrix() {
    var matrix = $("div.matrix").filter(visibility_filter);
    if(matrix.hasClass("regular"))
        return matrix;
}

function visibility_filter() {
    return $(this).parent().css("display")!="none";
}
/* Converts table into matrix for this point. This point (i,j and j,i) should be empty now.
 * Other points should be filled with values. It should work mainly based on the way we
 * fill the array.
 * We return just one array for the sake of simplicity. */
function convertTableToArray(table) {
    var id = table.attr("id");
    var result = [];
    var max = table.find("tr").length-1; // table will have N+1 rows
    for(var i=0; i<max; i++) {
        result[i] = [];
    }
    for(var i=0; i<max; i++) {
        for(var j=0; j<max; j++) {
            var name = "matrix["+id+"][data]["+i+"]["+j+"]";
            result[i][j] = $("div.matrix input[name='"+name+"']").val();
        }
    }
    return result;
}

function getBestMatrix(array, x, y) {
    // full array, now let's remove some elements.
    var max = array.length;
    var all_rows = [];
    for(var i=0; i<max; i++) {
        all_rows[i] = i;
    }
    if(isArrayPopulatedExcept(array, x,y,[])) // first: check if it's OK without removinf rows
            return remove_rows(array, []);
    for(var i=0; i<max-1; i++) {
        var permutations = generatePermutations(all_rows, i);
        // wszystkie kombinacje
        for(var j=0; j<permutations.length; j++) {
            var removed_rows = permutations[j];
            if(isArrayPopulatedExcept(array, x,y,removed_rows)
                    && $.inArray(x, removed_rows)==-1
                    && $.inArray(y, removed_rows)==-1) {
                return remove_rows(array, removed_rows);
            }
        }
    }
    return array;
}

function generatePermutations(array, n) {
    var result = [];
    for(var i=0; i<array.length; i++) {
        if(n>1) {
            var temp = array.slice();
            temp.splice(i,1);
            var elems = generatePermutations(temp, n-1);
            for(var j=0; j<elems.length; j++)
                elems[j].splice(0,0,array[i]); // add element at beginning to each
            result = result.concat(elems);
        }
        else if(n==0) { //only if user specified 0
            // do nothing
        }
        else
            result.push([array[i]]);
    }
    return result;
}

function valid_cell(cell) {
    return cell>0 && typeof cell != 'undefined';
}
/*
 * Checks whether this array is fully populated EXCEPT point we're looking for and excluded rows/columns.
 * Returns row/column that is unfilled.
 */
function isArrayPopulatedExcept(array, i,j,excluded_rows) {
    var max = array.length;
    for(var x=0; x<max; x++)
        for(var y=0; y<max; y++)
            if(($.inArray(x, excluded_rows)==-1 && $.inArray(y, excluded_rows)==-1
                    || excluded_rows.length==0)
                    && !((x==i && y==j) || (x==j && y==i)) 
                    && !valid_cell(array[x][y]))
                return false;
    return true;
}

function remove_rows(array, rows_to_remove) {
    var result = [];
    for(var i=0; i<array.length; i++) {
        if($.inArray(i, rows_to_remove)==-1) {
            var row = [];
            for(var j=0; j<array.length; j++) {
                if($.inArray(j, rows_to_remove)==-1)
                    row.push(array[i][j]);
            }
            result.push(row);
        }
    }
    return result;
}

function getBest(table, x,y) {
    if(typeof table == "undefined")
        table = getVisibleMatrix();
    if(typeof table == "undefined")
        return;
    var matrix = convertTableToArray(table);
    if(typeof matrix == "undefined")
        return;
    return getBestMatrix(matrix, x,y);
}

function is_valid_value_for_matrix(matrix_unfilled, value, left, N) {
    var matrix = fill_matrix_with_value(matrix_unfilled, value, left);
    return is_matrix_consistent(matrix, N);
}
function is_matrix_consistent(matrix, N) {
    if(matrix.length<2)
        return true;
    var s = []; //suma w kolumnach
    for(var i=0; i<matrix.length; i++) {
        for(var j=0; j<matrix.length; j++) {
            if(typeof s[j]=='undefined')
                s[j] = 0;
            s[j] += parseFloat(matrix[i][j]);
        }
    }
    // dzielimy przez sumÄ™
    for(var i=0; i<matrix.length; i++) {
        for(var j=0; j<matrix.length; j++)
            matrix[i][j] /= s[j];
    }
    // znormalizowana
    var lambda = 0;
    for(var i=0; i<matrix.length; i++) {
        var suma = 0;
        for(var j=0; j<matrix.length; j++) {
            suma += parseFloat(matrix[i][j]);
        }
        lambda += suma/matrix.length*s[i];
    }
    var CI = 1.0*(lambda - matrix.length)/(matrix.length - 1);
    var CR = CI/RI(N);
    return CR<0.10;
}

function RI(i) {
    switch(i) {
            case 3:
                    return 0.52;
            case 4:
                    return 0.89;
            case 5:
                    return 1.11;
            case 6:
                    return 1.25;
            case 7:
                    return 1.35;
            case 8:
                    return 1.40;
            case 9:
                    return 1.45;
            case 10:
                    return 1.49;
            case 11:
                    return 1.51;
            case 12:
                    return 1.54;
            case 13:
                    return 1.56;
            case 14:
                    return 1.57;
    }
    if(i>14)
            return 1.58;
    else
        return 10;
}

function fill_matrix_with_value(matrix_unfilled, value, left) { // left - if value should be supplied to left value (row)
    var matrix = [];
    for(var i = 0; i<matrix_unfilled.length; i++)
        matrix[i] = matrix_unfilled[i].slice(0);
    var value2 = 1.0/value;
    var index = findEmptyIndex(matrix_unfilled);
    if(index==null)
        alert("There was an error");
    else {
        if(left) {
            matrix[index[0]][index[1]] = value;
            matrix[index[1]][index[0]] = value2;
        }
        else {
            matrix[index[1]][index[0]] = value;
            matrix[index[0]][index[1]] = value2;
        }
    }
    return matrix;
}

function findEmptyIndex(matrix) {
    for(var i=0; i<matrix.length; i++) {
        for(var j=0; j<matrix.length; j++) {
            if(matrix[i][j]=="") {
                return [i,j];
            }
        }
    }
    return null;
}

