$("div.matrix.fuzzy input").click(fuzzyClicked);

function fuzzyClicked() {
    var input = $(this);
    var popup = window.open('popup_generate_fuzzy?val='+JSON.stringify(input.val()), '_blank', 'top=300, left=300, width=600, height=500');
    popup.onbeforeunload = function() {
    var val = $(this.document.documentElement).find("input[name='value']").val();
        if(val.length>0) {
            input.val(val);
        }
    }
}